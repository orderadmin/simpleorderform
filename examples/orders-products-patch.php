<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$orders = file('./examples/dbase/orders.txt');

foreach ($orders as $orderId) {
    $orderId = trim($orderId);
    
    $result = $api->request(
        Api::TYPE_GET, '/api/products/order/product', [
            'filter'   => [
                [
                    'field' => 'order',
                    'type'  => 'eq',
                    'value' => $orderId,
                ],
                [
                    'field' => 'state',
                    'type'  => 'eq',
                    'value' => 'active',
                ],
            ],
            'fields'   => [
                'id', 'state'
            ],
            'per_page' => 250,
        ]
    )->getResult();

    $helper->log(sprintf('Found %s order products for order %s', $result['total_items'], $orderId));
    if (empty($result['total_items'])) {
        continue;
    }

    $orderProducts = $result['_embedded']['order_product'];
    foreach ($orderProducts as $orderProduct) {
//    $helper->log(var_export($order, true));
        $patchUrl = parse_url($orderProduct['_links']['self']['href'], PHP_URL_PATH);

        $newState = 'active';
        $result = $api->setRequest(
            [
                'state' => $newState,
            ]
        )->request(
            Api::TYPE_PATCH,
            $patchUrl
        )->getResult();

        if ($result['state'] == $newState) {
            $helper->log(
                sprintf('Order %s: %s', $orderProduct['id'], $newState)
            );
        }
    }
}