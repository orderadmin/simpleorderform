<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$warehouse = 20;

$result = $api->request(
    Api::TYPE_GET, '/api/storage/place', [
        'filter'     => [
            [
                'field' => 'type',
                'type'  => 'eq',
                'value' => 'distribution',
            ],
//            [
//                'field' => 'id',
//                'type'  => 'gte',
//                'value' => 100,
//            ],
//            [
//                'field' => 'warehouse',
//                'type'  => 'eq',
//                'value' => $warehouse,
//            ],
        ],
        //        'fields'   => [
        //            'id', 'state', 'shipmentDate',
        //        ],
        'per_page'   => 500,
        'order-by' => [
            [
                'type'      => 'field',
                'field'     => 'created',
                'direction' => 'asc',
            ],
        ],
    ]
)->getResult();

$helper->log(sprintf('Found %s places', $result['total_items']));

$places = $result['_embedded']['place'];
foreach ($places as $place) {
    $result = $api->request(
        Api::TYPE_GET, '/api/storage/item', [
            'filter'     => [
                [
                    'field' => 'partition',
                    'type'  => 'eq',
                    'value' => $place['_embedded']['warehouse']['id'],
                ],
                [
                    'field' => 'place',
                    'type'  => 'eq',
                    'value' => $place['id'],
                ],
            ],
            'per_page'   => 150,
        ]
    )->getResult();

    $helper->log(sprintf('Location %s has %s items', $place['id'], $result['total_items']));

    $prevReserve = null;
    if ($result['total_items'] > 0) {
        $items = $result['_embedded']['item'];
        foreach ($items as $item) {
            if ($item['_embedded']['reserve']['state'] === 'complete') {
                $reserve = $item['_embedded']['reserve'];

                if ($prevReserve != $reserve['id']) {
                    $helper->log(
                        sprintf('Reserve %s complete', $reserve['id'])
                    );

                    $newState = $reserve['state'];

                    $result = $api->setRequest(
                        [
                            'state' => $newState
                        ]
                    )->request(
                        Api::TYPE_PATCH,
                        sprintf('/api/storage/reserve/%s', $reserve['id'])
                    )->getResult();

                    if ($result['state'] == $newState) {
                        $helper->log(
                            sprintf('Reserve %s: %s', $reserve['id'], $newState)
                        );
                    }

                    $prevReserve = $reserve['id'];
                }
            }
        }
    }
}
