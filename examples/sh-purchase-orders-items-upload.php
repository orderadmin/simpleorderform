<?php

namespace App;

use App\Lib\Api;
use GraphQL\Client;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use function Symfony\Component\String\s;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$client = new Client(
    'https://public-api.shiphero.com/graphql',
    ['Authorization' => 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlJUQXlOVU13T0Rrd09ETXhSVVZDUXpBNU5rSkVOVVUxUmtNeU1URTRNMEkzTWpnd05ERkdNdyJ9.eyJodHRwOi8vc2hpcGhlcm8tcHVibGljLWFwaS91c2VyaW5mbyI6eyJuYW1lIjoiU2VyZ2V5IEFkdXNoZWV2IiwiZmlyc3RfbmFtZSI6IlNlcmdleSIsImxhc3RfbmFtZSI6IkFkdXNoZWV2Iiwibmlja25hbWUiOiJzdGFmZiIsInBpY3R1cmUiOiJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci8yMTUwOWIwZmNmZDhiMmFkZDUyMmJlYjcyNTg3MTgxNz9zPTQ4MCZyPXBnJmQ9aHR0cHMlM0ElMkYlMkZjZG4uYXV0aDAuY29tJTJGYXZhdGFycyUyRnNhLnBuZyIsImFjY291bnRfaWQiOjU5Mzg0LCJpc19hY2NvdW50X2FkbWluIjpmYWxzZX0sImlzcyI6Imh0dHBzOi8vbG9naW4uc2hpcGhlcm8uY29tLyIsInN1YiI6ImF1dGgwfDVmYzdiZDE0ZWRhODZhMDA2OTY3ZGUwMiIsImF1ZCI6WyJzaGlwaGVyby1wdWJsaWMtYXBpIiwiaHR0cHM6Ly9zaGlwaGVyby5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNjk4ODQ3NDQzLCJleHAiOjE3MDEyNjY2NDMsImF6cCI6Im10Y2J3cUkycjYxM0RjT04zRGJVYUhMcVF6UTRka2huIiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSB2aWV3OnByb2R1Y3RzIGNoYW5nZTpwcm9kdWN0cyB2aWV3Om9yZGVycyBjaGFuZ2U6b3JkZXJzIHZpZXc6cHVyY2hhc2Vfb3JkZXJzIGNoYW5nZTpwdXJjaGFzZV9vcmRlcnMgdmlldzpzaGlwbWVudHMgY2hhbmdlOnNoaXBtZW50cyB2aWV3OnJldHVybnMgY2hhbmdlOnJldHVybnMgdmlldzp3YXJlaG91c2VfcHJvZHVjdHMgY2hhbmdlOndhcmVob3VzZV9wcm9kdWN0cyB2aWV3OnBpY2tpbmdfc3RhdHMgdmlldzpwYWNraW5nX3N0YXRzIG9mZmxpbmVfYWNjZXNzIiwiZ3R5IjoicGFzc3dvcmQifQ.E5YTLMR4OHQph-68jbPzXI50ujFJdAH-Pq0Ht7tTwwFgpA0qtWTS-CAPY3mGElMWo0dRAfmJRSGMJ6eKolhAppuU1uCAJJ59nEv2H_1dc_Jbt-tfITl2P4Kk3T5RI40CLfklGhJEBgY2EyWx0dgNtpv6uxUmom18YF7NTMxX7XA1NkfxhhNyPrReoTvxopJ8Ae5T1cZpCrPZOu1e1bfrMobzuyX271kBYcJ0hu447xpsXGeGfYz15_5I5gum5xpo572D5LL_uzR-fL0l5mY38WVsWFE3sp-7sOjisaD4b-0xBbMheqiO_HsjnjzmnutMt5viCiFDbtJgDG9WFG-5vg']
);

$pages = 1;
for ($page = 1; $page <= $pages; $page++) {
    $poResult = $api->request(
        Api::TYPE_GET, '/api/storage/movements/document', [
            'filter'   => [
                [
                    'type'  => 'eq',
                    'field' => 'warehouse',
                    'value' => 13187,
                ]
            ],
            'page'     => $page,
            'per_page' => 250,
        ]
    )->getResult();

    if ($pages <> $poResult['page_count']) {
        $pages = $poResult['page_count'];
    }

    foreach ($poResult['_embedded']['movement_acceptance'] as $purchaseOrder) {
        $purchaseOrderId = $purchaseOrder['id'];
        if (empty($purchaseOrder['raw'])) {
            continue;
        }

        $shPurchaseOrderId = $purchaseOrder['raw']['id'];
        if (!empty($purchaseOrder['raw']['line_items'])) {
            continue;
        }

        $gql = <<<QUERY
query {
  purchase_order(id: "$shPurchaseOrderId") {
    request_id
    complexity
    data {
      id
      legacy_id
      po_number
      account_id
      warehouse_id
      vendor {
            id
            legacy_id
            name
            email
            account_number
            account_id
            address {
              name
              address1
              address2
              city
              state
              country
              zip
              phone
            }
            currency
            internal_note
            default_po_note
            logo
            partner_vendor_id
            created_at
          }
      created_at
      po_date
      date_closed
      packing_note
      fulfillment_status
      po_note
      description
      subtotal
      discount
      total_price
      tax
      shipping_method
      shipping_carrier
      shipping_name
      shipping_price
      tracking_number
      pdf
      images
      payment_method
      payment_due_by
      payment_note
      locking
      locked_by_user_id
      line_items(first: 150) {
        edges {
          node {
            id
            legacy_id
            po_id
            account_id
            warehouse_id
            vendor_id
            po_number
            sku
            barcode
            vendor_sku
            product_id
            variant_id
            quantity
            quantity_received
            quantity_rejected
            price
            product_name
            option_title
            expected_weight_in_lbs
            fulfillment_status
            sell_ahead
            note
            partner_line_item_id
            updated_at
            created_at
          }
        }
      }
    }
  }
}
QUERY;

        $res = $client->runRawQuery($gql);

        $purchaseOrderData = $res->getData();
        $purchaseOrderData = json_decode(json_encode($purchaseOrderData), true);

        $purchaseOrdersData
            = $purchaseOrderData['purchase_order']['data'];

        $helper->log(
            sprintf('Purchase orders found: %s', count($purchaseOrderData))
        );

        $result = $api->setRequest([
            'raw' => $purchaseOrdersData,
        ])->request(
            Api::TYPE_PATCH,
            sprintf('/api/storage/movements/document/%s', $purchaseOrderId)
        )->getResult();

        $helper->log(
            sprintf('Purchase order %s was updated', $result['id'])
        );
    }
}
