<?php

namespace App;

use App\Lib\Api;
use GraphQL\Client;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use function Symfony\Component\String\s;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$pages = 1;
for ($page = 1; $page <= $pages; $page++) {
    $result = $api->request(
        Api::TYPE_GET, '/api/storage/movements/document', [
            'filter'   => [
                [
                    'type'  => 'eq',
                    'field' => 'warehouse',
                    'value' => 13187,
                ],
                [
                    'type'  => 'eq',
                    'field' => 'state',
                    'value' => 'new',
                ],
                [
                    'type'  => 'isNotNull',
                    'field' => 'raw',
                ],
            ],
            'per_page' => 250,
        ]
    )->getResult();

    $helper->log(sprintf('Found %s purchase orders', $result['total_items']));

    $pages = $result['page_count'];

    $purchaseOrdersData = $result['_embedded']['movement_acceptance'];
    foreach ($purchaseOrdersData as $purchaseOrderData) {
        $helper->log(
            sprintf('Working with purchase order %s', $purchaseOrderData['id'])
        );
//        echo json_encode($purchaseOrderData, JSON_PRETTY_PRINT);

        $result = $api->request(
            Api::TYPE_GET, '/api/storage/movements/document/item', [
                'filter'   => [
                    [
                        'type'  => 'eq',
                        'field' => 'document',
                        'value' => $purchaseOrderData['id'],
                    ],
                ],
                'per_page' => 250,
            ]
        )->getResult();

        if (empty($result['page_count'])) {
            if (empty($purchaseOrderData['raw']['line_items']['edges'])) {
                $helper->log('Lines are empty');

                $patchUrl = parse_url(
                    $purchaseOrderData['_links']['self']['href'],
                    PHP_URL_PATH
                );

                $res = $api->setRequest([
                    'state' => 'closed',
                ])->request(Api::TYPE_PATCH, $patchUrl)->getResult();

                continue;
            } else {
                $helper->log(
                    sprintf(
                        'Found "%s" items',
                        count($purchaseOrderData['raw']['line_items']['edges'])
                    )
                );
            }

            foreach (
                $purchaseOrderData['raw']['line_items']['edges'] as $lineItem
            ) {
                $lineItem = $lineItem['node'];

                $offer = [
                    'shop'     => 202973,
                    'type'     => 'simple',
                    'name'     => $lineItem['product_name'],
                    'sku'      => $lineItem['sku'],
                    'barcodes' => [
                        $lineItem['barcode'],
                    ],
                    'raw'      => $lineItem,
                    'eav'      => [
                        'source' => 'spo',
                    ],
                ];

                try {
                    $result = $api->request(
                        Api::TYPE_GET, '/api/products/offer', [
                            'filter'   => [
                                [
                                    'field' => 'shop',
                                    'type'  => 'eq',
                                    'value' => $offer['shop'],
                                ],
                                [
                                    'field' => 'sku',
                                    'type'  => 'eq',
                                    'value' => $offer['sku'],
                                ],
                            ],
                            'fields'   => [
                                'id', 'shop', 'sku', 'article',
                            ],
                            'per_page' => 250,
                        ]
                    )->getResult();

                    $helper->log(
                        sprintf(
                            'Found %s offers',
                            $result['total_items']
                        )
                    );

                    if (empty($result['total_items'])) {
                        $offer = $api->setRequest($offer)->request(
                            Api::TYPE_POST, '/api/products/offer'
                        )->getResult();

                        $helper->log(
                            sprintf('Added offer %s', $offer['id'])
                        );
                    } else {
                        if ($result['total_items'] === 1) {
                            $offer = $result['_embedded']['product_offer'][0];
                        } else {
                            var_dump($result);
                            die();
                        }
                    }
                } catch (\Exception $e) {
                    $helper->log($e->getMessage());

                    continue;
                }

                $item = [
                    'document'         => $purchaseOrderData['id'],
                    'shop'             => $offer['_embedded']['shop']['id'],
                    'productOffer'     => [
                        'id'   => $offer['id'],
                        'shop' => $offer['_embedded']['shop']['id'],
                    ],
                    'purchasingPrice'  => $lineItem['price'],
                    'quantityExpected' => $lineItem['quantity'],
                    'sku'              => $lineItem['barcode'],
                    'comment'          => $lineItem['note'],
                    'eav'              => $lineItem,
                ];

                $result = $api->setRequest($item)->request(
                    Api::TYPE_POST, '/api/storage/movements/document/item'
                )->getResult();

                $helper->log(
                    sprintf(
                        'Added item with id %s',
                        $result['id']
                    )
                );
            }

            if (!empty($purchaseOrderData['raw']['fulfillment_status'])) {
                $state = null;
                switch ($purchaseOrderData['raw']['fulfillment_status']) {
                    case 'closed':
                        $state = 'closed';
                        break;
                }

                if (!empty($state)) {
                    $patchUrl = parse_url(
                        $purchaseOrderData['_links']['self']['href'],
                        PHP_URL_PATH
                    );

                    $api->setRequest([
                        'state' => $state,
                    ])->request(Api::TYPE_PATCH, $patchUrl)->getResult();
                }
            }
        } else {
//            echo json_encode($purchaseOrderData, JSON_PRETTY_PRINT);

            $helper->log('Next');

            if (count($purchaseOrderData['raw']['line_items']['edges']) === $result['total_items']) {
                $state = null;
                switch ($purchaseOrderData['raw']['fulfillment_status']) {
                    case 'closed':
                        $state = 'closed';
                        break;

                    case 'canceled':
                        $state = 'draft';
                        break;

                    case 'pending':
                        $dateString = $purchaseOrderData['raw']['po_date'];

                        $poDate = new \DateTime($dateString);

                        $targetDate = new \DateTime('2023-10-01');

                        if ($poDate < $targetDate) {
                            $state = 'draft';
                        } else {
                            $state = null;
                        }
                        break;

                    default:
                        echo  $purchaseOrderData['raw']['fulfillment_status'];die();
                        break;
                }

                if (!empty($state)) {
                    $patchUrl = parse_url(
                        $purchaseOrderData['_links']['self']['href'],
                        PHP_URL_PATH
                    );

                    $res = $api->setRequest([
                        'state' => $state,
                    ])->request(Api::TYPE_PATCH, $patchUrl)->getResult();
                }
            }

            continue;
        }
    }
}
