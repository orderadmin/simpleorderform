<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$result = $api->request(
    Api::TYPE_GET, '/api/delivery-services/requests', [
        'filter'   => [
            [
                'field' => 'sender',
                'type'  => 'eq',
                'value' => 16733,
            ],
            [
                'field' => 'state',
                'type'  => 'eq',
                'value' => 'pending',
            ],
            [
                'field' => 'trackingNumber',
                'type'  => 'isNull',
            ],
        ],
        'per_page' => 250,
        'fields'   => [
            'id',
            'state',
            'order',
        ],
    ]
)->getResult();

$helper->log(sprintf('Found %s delivery requests', $result['total_items']));

$count = 0;
$deliveryRequests = $result['_embedded']['delivery_requests'];
foreach ($deliveryRequests as $deliveryRequest) {
    if (!empty($deliveryRequest['order'])) {
        $order = json_decode($deliveryRequest['order'], true);
    } else {
        $helper->log(
            sprintf(
                'Delivery request %s has na empty order', $deliveryRequest['id']
            )
        );

        continue;
    }

    if ($order['state'] !== 'assembled') {
        $helper->log(
            sprintf(
                'Delivery request %s order is not assembled', $deliveryRequest['id']
            )
        );

        continue;
    }

    $helper->log(
        sprintf(
            'Delivery request %s', $deliveryRequest['id']
        )
    );

    $patchUrl = parse_url(
        $deliveryRequest['_links']['self']['href'], PHP_URL_PATH
    );
    $helper->log(sprintf('Patch URL: %s', $patchUrl));

    $result = $api->setRequest(
        [
            'rate' => 341,
        ]
    )->request(Api::TYPE_PATCH, $patchUrl)->getResult();

    $result = $api->setRequest(
        [
            'queue'           => 257,
            'state'           => 'measured',
            'deliveryRequest' => [
                'sendDate'   => 'Thu Aug 15 2024',
                'weight'     => 0.85,
                'dimensions' => [
                    'x' => 13,
                    'y' => 10,
                    'z' => 2,
                ],
                'id'         => $deliveryRequest['id'],
                //                'state' => 'pending',
            ],
        ]
    )->request(Api::TYPE_POST, '/api/delivery-services/preprocessing-task')
        ->getResult();

    if (!empty($result)) {
        $backState = 'pending';

        $result = $api->setRequest(
            [
                'state' => $backState,
            ]
        )->request(Api::TYPE_PATCH, $patchUrl)->getResult();

        if ($result['state'] == $backState) {
            $helper->log(
                sprintf(
                    'Delivery request %s state is: %s',
                    $deliveryRequest['id'], $backState
                )
            );
        }
    }

    if ($count++ === 10) {
        die();
    }
}