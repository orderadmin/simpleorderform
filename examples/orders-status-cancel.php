<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$result = $api->request(
    Api::TYPE_GET, '/api/products/order', [
        'filter'   => [
            [
                'field' => 'source',
                'type'  => 'eq',
                'value' => 9032,
            ],
            [
                'field'  => 'state',
                'type'   => 'notIn',
                'values' => ['pending_queued', 'download', 'partly_reserved', 'cancel',],
            ],
            [
                'field' => 'id',
                'type'  => 'neq',
                'value' => '31135070',
            ]
        ],
        'fields'   => [
            'id', 'state', 'shipmentDate',
        ],
        'per_page' => 250,
        'order-by' => [
            [
                'type'      => 'field',
                'field'     => 'shipmentDate',
                'direction' => 'asc',
            ],
        ],
    ]
)->getResult();

$helper->log(sprintf('Found %s orders', $result['total_items']));

$orders = $result['_embedded']['order'];
foreach ($orders as $order) {
//    $helper->log(var_export($order, true));
    $newState = 'cancel';

    $result = $api->setRequest(
        [
            'state' => $newState
        ]
    )->request(
        Api::TYPE_PATCH,
        sprintf('/api/products/order/%s', $order['id'])
    )->getResult();

    if ($result['state'] == $newState) {
        $helper->log(
            sprintf('Order %s: %s', $order['id'], $newState)
        );
    }
}