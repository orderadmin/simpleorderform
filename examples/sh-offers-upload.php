<?php

namespace App;

use App\Lib\Api;
use GraphQL\Client;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use function Symfony\Component\String\s;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$client = new Client(
    'https://public-api.shiphero.com/graphql',
    ['Authorization' => 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlJUQXlOVU13T0Rrd09ETXhSVVZDUXpBNU5rSkVOVVUxUmtNeU1URTRNMEkzTWpnd05ERkdNdyJ9.eyJodHRwOi8vc2hpcGhlcm8tcHVibGljLWFwaS91c2VyaW5mbyI6eyJuYW1lIjoiU2VyZ2V5IEFkdXNoZWV2IiwiZmlyc3RfbmFtZSI6IlNlcmdleSIsImxhc3RfbmFtZSI6IkFkdXNoZWV2Iiwibmlja25hbWUiOiJzdGFmZiIsInBpY3R1cmUiOiJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci8yMTUwOWIwZmNmZDhiMmFkZDUyMmJlYjcyNTg3MTgxNz9zPTQ4MCZyPXBnJmQ9aHR0cHMlM0ElMkYlMkZjZG4uYXV0aDAuY29tJTJGYXZhdGFycyUyRnNhLnBuZyIsImFjY291bnRfaWQiOjU5Mzg0LCJpc19hY2NvdW50X2FkbWluIjpmYWxzZX0sImlzcyI6Imh0dHBzOi8vbG9naW4uc2hpcGhlcm8uY29tLyIsInN1YiI6ImF1dGgwfDVmYzdiZDE0ZWRhODZhMDA2OTY3ZGUwMiIsImF1ZCI6WyJzaGlwaGVyby1wdWJsaWMtYXBpIiwiaHR0cHM6Ly9zaGlwaGVyby5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNzAxMzA1OTU2LCJleHAiOjE3MDM3MjUxNTYsImF6cCI6Im10Y2J3cUkycjYxM0RjT04zRGJVYUhMcVF6UTRka2huIiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSB2aWV3OnByb2R1Y3RzIGNoYW5nZTpwcm9kdWN0cyB2aWV3Om9yZGVycyBjaGFuZ2U6b3JkZXJzIHZpZXc6cHVyY2hhc2Vfb3JkZXJzIGNoYW5nZTpwdXJjaGFzZV9vcmRlcnMgdmlldzpzaGlwbWVudHMgY2hhbmdlOnNoaXBtZW50cyB2aWV3OnJldHVybnMgY2hhbmdlOnJldHVybnMgdmlldzp3YXJlaG91c2VfcHJvZHVjdHMgY2hhbmdlOndhcmVob3VzZV9wcm9kdWN0cyB2aWV3OnBpY2tpbmdfc3RhdHMgdmlldzpwYWNraW5nX3N0YXRzIG9mZmxpbmVfYWNjZXNzIiwiZ3R5IjoicGFzc3dvcmQifQ.YNxXnL9tOdvlNDCvoLJQbKBGRqRcBN8rFUZQ4rJ9OflNlv6u4Htd26zESW1QsVXhbAH_W2CfWV9YO1HgeB3YNrx6-m77FZNFVjrwwboquvYp8U8CdplG93CBRPYTz78RiCY6dLHyI_CPo0bmg9pHX67lVtKQpaFq1Mq-YFuUCufDzi63ZdiEUVTRdk2qEUw1WJYjdPzFrayAmzqXrT6plob6_g0KCQ5DXqdZVmyrC2XrqLoqhKvGtfu8F6WWIg1Jhh_6TSIj3xHwiwxtQKRqrWp6uMmiictdnTjSXDRjJouY06KH2QeP1cfMJS06RsnEyRsjvpmeM4iq78tyCq435Q']
);

$cnt = 0;
$i = 169;
$hasNextPage = true;
$perPage = 1000;
$lastId = 'YXJyYXljb25uZWN0aW9uOjIwOTQ5OQ==';
while ($hasNextPage) {
    $i++;

    $first = $i * $perPage;

    $helper->log(
        sprintf(
            'Query: %s per page, last id is "%s", page %s', $perPage,
            $lastId ?? '-', $i
        )
    );
    if (!empty($lastId)) {
        $lastId = sprintf(' after: "%s"', $lastId);
    }

    $gql = <<<QUERY
query {
  products {
    request_id
    complexity
    data(first: $perPage$lastId) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          legacy_id
          account_id
          name
          sku
          barcode
          country_of_manufacture
          dimensions {
            height
            width
            length
            weight
          }
          tariff_code
          kit
          kit_build
          no_air
          final_sale
          customs_value
          customs_description
          not_owned
          dropship
          needs_serial_number
          thumbnail
          large_thumbnail
          created_at
          updated_at
          product_note
          virtual
          ignore_on_invoice
          ignore_on_customs
          active
          warehouse_products {
            warehouse_id
            on_hand
          }
          images {
            src
          }
          tags
          kit_components {
            sku
            quantity
          }
        }
      }
    }
  }
}
QUERY;

    $res = $client->runRawQuery($gql);

    $offersData = $res->getData();
    $offersData = json_decode(json_encode($offersData), true);

    $pageInfo = $offersData['products']['data']['pageInfo'];
    if (!empty($pageInfo['hasNextPage'])) {
        $cnt++;
        $lastId = $pageInfo['endCursor'];
    } else {
        $lastId = null;
        $hasNextPage = false;
    }

    $offersData = $offersData['products']['data']['edges'];

    $res = json_encode($offersData, JSON_PRETTY_PRINT);
    file_put_contents(sprintf('%s/sh/offers_%s.json', __DIR__, $i), $res);

    foreach ($offersData as $offerData) {
//        echo json_encode($offerData, JSON_PRETTY_PRINT);

        $offerData = $offerData['node'];

        $offer = [
            'shop'     => 202973,
            'type'     => 'simple',
            'extId'    => $offerData['id'],
            'name'     => $offerData['name'],
            'image'    => $offerData['large_thumbnail'],
            'sku'      => $offerData['sku'],
            'barcodes' => [
                $offerData['barcode'],
            ],
            'raw'      => $offerData,
            'eav'      => [
                'source' => 's',
            ],
        ];

        try {
            $result = $api->request(
                Api::TYPE_GET, '/api/products/offer', [
                    'filter'   => [
                        [
                            'field' => 'shop',
                            'type'  => 'eq',
                            'value' => $offer['shop'],
                        ],
                        [
                            'field' => 'sku',
                            'type'  => 'eq',
                            'value' => $offer['sku'],
                        ],
                    ],
                    'fields'   => [
                        'id', 'shop', 'sku', 'article',
                    ],
                    'per_page' => 250,
                ]
            )->getResult();

            $helper->log(
                sprintf(
                    'Found %s offers, cnt %s, last id "%s", page %s',
                    $result['total_items'], $cnt, $lastId, $i
                )
            );
            if (empty($result['total_items'])) {
                $result = $api->setRequest($offer)->request(
                    Api::TYPE_POST, '/api/products/offer', [
                        'filter'   => [
                            [
                                'field' => 'shop',
                                'type'  => 'eq',
                                'value' => $offer['shop'],
                            ],
                            [
                                'field' => 'sku',
                                'type'  => 'eq',
                                'value' => $offer['sku'],
                            ],
                        ],
                        'fields'   => [
                            'id', 'shop', 'sku', 'article',
                        ],
                        'per_page' => 250,
                    ]
                )->getResult();

                $helper->log(
                    sprintf('Added offer %s', $result['id'])
                );
            }
        } catch (\Exception $e) {
            $helper->log($e->getMessage());

            continue;
        }
    }
}



//$db = new \SQLite3('./examples/dbase/data-offers.db');
//$db->exec('PRAGMA journal_mode = wal;');

//$res = file_get_contents($config['shiphero-snapshot']);
//$res = json_decode($res, true);
//
//echo json_encode($res, JSON_PRETTY_PRINT);
//
//die();
//
//$totalPages = 1;
//for ($page = 1; $page <= $totalPages; $page++) {
//    $result = $api->request(
//        Api::TYPE_GET, '/api/products/offer', [
//            'filter'   => [
//                [
//                    'field' => 'shop',
//                    'type'  => 'eq',
//                    'value' => 177307,
//                ],
//                [
//                    'field' => 'name',
//                    'type'  => 'isNull',
//                ],
//            ],
//            'fields'   => [
//                'id', 'shop', 'sku', 'article',
//            ],
//            'page'     => $page,
//            'per_page' => 250,
//            'order-by' => [
//                [
//                    'type'      => 'field',
//                    'field'     => 'id',
//                    'direction' => 'asc',
//                ],
//            ],
//        ]
//    )->getResult();
//
//    $helper->log(sprintf('Found %s offers', $result['total_items']));
//
//    if ($page == 1) {
//        $totalPages = $result['page_count'];
//
//        $helper->log(sprintf('Found %s pages', $totalPages));
//    }
//
//    $productOffers = $result['_embedded']['product_offer'];
//    foreach ($productOffers as $productOffer) {
////    $helper->log(var_export($productOffer, true));
//
//        $productOffer = $api->request(
//            Api::TYPE_GET, sprintf(
//                '/api/products/offer/%s/%s',
//                $productOffer['_embedded']['shop']['id'], $productOffer['id']
//            )
//        )->getResult();
//
//        if (empty($productOffer['eav']['integrations-amazon-offer-asin'])) {
//            $helper->log(
//                sprintf('Product offer %s, have no ASIN', $productOffer['id'])
//            );
//
//            continue;
//        }
//
//        try {
//            $result = $api->setRequest(
//                [
//                    'eav' => [
//                        'integrations-amazon-offer-asin' => [
//                            'ATVPDKIKX0DER' => $productOffer['eav']['integrations-amazon-offer-asin']['ATVPDKIKX0DER'],
//                        ],
//                    ],
//                ]
//            )->request(
//                Api::TYPE_PATCH,
//                sprintf(
//                    '/api/products/offer/%s/%s',
//                    $productOffer['_embedded']['shop']['id'],
//                    $productOffer['id']
//                )
//            )->getResult();
//        } catch (\Exception $e) {
//            $helper->log($e->getMessage());
//        }
//
//        $helper->log(
//            sprintf(
//                'Updated product offer %s, ASIN: %s (page %s)',
//                $productOffer['id'],
//                $result['eav']['integrations-amazon-offer-asin']['ATVPDKIKX0DER'], $page
//            )
//        );
//    }
//}
