<?php

namespace App;

use App\Lib\Api;
use GraphQL\Client;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use function Symfony\Component\String\s;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$client = new Client(
    'https://public-api.shiphero.com/graphql',
    ['Authorization' => 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlJUQXlOVU13T0Rrd09ETXhSVVZDUXpBNU5rSkVOVVUxUmtNeU1URTRNMEkzTWpnd05ERkdNdyJ9.eyJodHRwOi8vc2hpcGhlcm8tcHVibGljLWFwaS91c2VyaW5mbyI6eyJuYW1lIjoiU2VyZ2V5IEFkdXNoZWV2IiwiZmlyc3RfbmFtZSI6IlNlcmdleSIsImxhc3RfbmFtZSI6IkFkdXNoZWV2Iiwibmlja25hbWUiOiJzdGFmZiIsInBpY3R1cmUiOiJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci8yMTUwOWIwZmNmZDhiMmFkZDUyMmJlYjcyNTg3MTgxNz9zPTQ4MCZyPXBnJmQ9aHR0cHMlM0ElMkYlMkZjZG4uYXV0aDAuY29tJTJGYXZhdGFycyUyRnNhLnBuZyIsImFjY291bnRfaWQiOjU5Mzg0LCJpc19hY2NvdW50X2FkbWluIjpmYWxzZX0sImlzcyI6Imh0dHBzOi8vbG9naW4uc2hpcGhlcm8uY29tLyIsInN1YiI6ImF1dGgwfDVmYzdiZDE0ZWRhODZhMDA2OTY3ZGUwMiIsImF1ZCI6WyJzaGlwaGVyby1wdWJsaWMtYXBpIiwiaHR0cHM6Ly9zaGlwaGVyby5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNjk4ODQ3NDQzLCJleHAiOjE3MDEyNjY2NDMsImF6cCI6Im10Y2J3cUkycjYxM0RjT04zRGJVYUhMcVF6UTRka2huIiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSB2aWV3OnByb2R1Y3RzIGNoYW5nZTpwcm9kdWN0cyB2aWV3Om9yZGVycyBjaGFuZ2U6b3JkZXJzIHZpZXc6cHVyY2hhc2Vfb3JkZXJzIGNoYW5nZTpwdXJjaGFzZV9vcmRlcnMgdmlldzpzaGlwbWVudHMgY2hhbmdlOnNoaXBtZW50cyB2aWV3OnJldHVybnMgY2hhbmdlOnJldHVybnMgdmlldzp3YXJlaG91c2VfcHJvZHVjdHMgY2hhbmdlOndhcmVob3VzZV9wcm9kdWN0cyB2aWV3OnBpY2tpbmdfc3RhdHMgdmlldzpwYWNraW5nX3N0YXRzIG9mZmxpbmVfYWNjZXNzIiwiZ3R5IjoicGFzc3dvcmQifQ.E5YTLMR4OHQph-68jbPzXI50ujFJdAH-Pq0Ht7tTwwFgpA0qtWTS-CAPY3mGElMWo0dRAfmJRSGMJ6eKolhAppuU1uCAJJ59nEv2H_1dc_Jbt-tfITl2P4Kk3T5RI40CLfklGhJEBgY2EyWx0dgNtpv6uxUmom18YF7NTMxX7XA1NkfxhhNyPrReoTvxopJ8Ae5T1cZpCrPZOu1e1bfrMobzuyX271kBYcJ0hu447xpsXGeGfYz15_5I5gum5xpo572D5LL_uzR-fL0l5mY38WVsWFE3sp-7sOjisaD4b-0xBbMheqiO_HsjnjzmnutMt5viCiFDbtJgDG9WFG-5vg']
);

$cnt = 0;
$i = 0;
$hasNextPage = true;
$perPage = 1500;
$lastId = null;
while ($hasNextPage) {
    $i++;

    $first = $i * $perPage;

    $helper->log(
        sprintf(
            'Query: %s per page, last id is "%s", page %s', $perPage,
            $lastId ?? '-', $i
        )
    );
    if (!empty($lastId)) {
        $lastId = sprintf(' after: "%s"', $lastId);
    }

    $helper->log("data(first: $perPage$lastId)");

    $gql = <<<QUERY
query {
  vendors {
    request_id
    complexity
    data(first: $perPage$lastId) {
      c
      edges {
        node {
          id
          legacy_id
          name
          email
          account_number
          account_id
          address {
            name
            address1
            address2
            city
            state
            country
            zip
            phone
          }
          currency
          internal_note
          default_po_note
          logo
          partner_vendor_id
          created_at
        }
      }
    }
  }
}
QUERY;

    $res = $client->runRawQuery($gql);

    $vendorsData = $res->getData();
    $vendorsData = json_decode(json_encode($vendorsData), true);

    $pageInfo = $vendorsData['vendors']['data']['pageInfo'];
    if (!empty($pageInfo['hasNextPage'])) {
        $lastId = $pageInfo['endCursor'];
    } else {
        $lastId = null;
        $hasNextPage = false;
    }

    $vendorsData = $vendorsData['vendors']['data']['edges'];

    $helper->log(sprintf('Vendors found: %s', count($vendorsData)));

    $res = json_encode($vendorsData, JSON_PRETTY_PRINT);
    file_put_contents(sprintf('%s/sh/vendors_%s.json', __DIR__, $i), $res);

    foreach ($vendorsData as $vendorData) {
        $vendorData = $vendorData['node'];

        $legalEntityData = [
            'owner' => 84760,
            'extId' => $vendorData['id'],
            'name'  => $vendorData['name'],
            'type'  => 'customer',
            'raw'   => $vendorData,
        ];

        $result = $api->request(
            Api::TYPE_GET, '/api/accounts/legal-entity', [
                'filter'   => [
                    [
                        'field' => 'extId',
                        'type'  => 'eq',
                        'value' => $legalEntityData['extId'],
                    ],
                ],
                'per_page' => 250,
            ]
        )->getResult();

        $helper->log(
            sprintf(
                'Found %s vendors, count %s, page %s, currrent id "%s", last id "%s"',
                $result['total_items'],
                $cnt, $i, $legalEntityData['extId'], $lastId
            )
        );

        $cnt++;

        if (empty($result['total_items'])) {
            $result = $api->setRequest($legalEntityData)->request(
                Api::TYPE_POST, '/api/accounts/legal-entity'
            )->getResult();

            $helper->log(
                sprintf('Added vendor %s', $result['id'])
            );
        }
    }
}
