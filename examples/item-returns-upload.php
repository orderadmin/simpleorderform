<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$file = './examples/dev-data/item-return.xlsx';
if (!empty($file)) {
    $reader = new Xlsx();
    $spreadsheet = $reader->load($file);

    $helper = new Sample();
    foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
        $helper->log('Worksheet - ' . $worksheet->getTitle());
        if ($worksheet->getTitle() !== 'Wave 2') {
            continue;
        }

        $dataKeys = [];
        foreach ($worksheet->getRowIterator() as $row) {
            $helper->log('    Row number - ' . $row->getRowIndex());

            if ($row->getRowIndex() < 5) {
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(
                false
            ); // Loop all cells, even if it is not set

            $i = 0;
            $data = [];
            foreach ($cellIterator as $cell) {
                if ($cell !== null) {
//                    $helper->log(
//                        '        Cell - ' . $cell->getCoordinate() . ' - '
//                        . $cell->getValue()
//                    );

                    $data[$cell->getColumn()] = $cell->getValue();
                }
            }

            $data = array_filter($data);
            if (empty($data['E'])) {
                continue;
            }

            $returnData = [
                'state' => 'new',
                'extId' => $data['E'],
                'raw'   => $data,
            ];

//            if (!in_array($returnData['extId'], [
//                'X003D6Z775',
//                'X003RB9V91',
//            ])) {
//                continue;
//            }

            $result = $api->request(
                Api::TYPE_GET, '/api/storage/item-return', [
                'filter' => [
                    [
                        'field' => 'extId',
                        'type'  => 'eq',
                        'value' => $returnData['extId'],
                    ]
                ],
            ], false
            )->getResult();

            if ($result['total_items'] == 0) {
                $result = $api->setRequest($returnData)->request(
                    Api::TYPE_POST, '/api/storage/item-return'
                )->getResult();

                $helper->log(
                    sprintf(
                        'Created return: %s', $result['id']
                    )
                );
            } else {
                $poResult = $api->request(
                    Api::TYPE_GET, '/api/products/offer', [
                    'filter' => [
                        [
                            'field' => 'shop',
                            'type'  => 'eq',
                            'value' => 203481,
                        ],
                    ],
                    'search' => $data['C'],
                ], false
                )->getResult();

                if ($poResult['total_items'] === 0) {
//                    $helper->log(var_export($result, true));
                    $helper->log(var_export($poResult, true));
                } else {
                    if ($poResult['total_items'] > 1) {
                        $poItemsResult = $api->request(
                            Api::TYPE_GET, '/api/products/offer', [
                            'filter' => [
                                [
                                    'field' => 'shop',
                                    'type'  => 'eq',
                                    'value' => 203481,
                                ],
                                [
                                    'field' => 'items',
                                    'type'  => 'gte',
                                    'value' => 1,
                                ],
                            ],
                            'search' => $data['C'],
                        ], false
                        )->getResult();

                        if ($poItemsResult['total_items'] === 1) {
                            $poResult = $poItemsResult;
                        }
                    }

//                    $helper->log(var_export($result, true));
                    $offers = $poResult['_embedded']['product_offer'];
                    $offer = $offers[0];

                    $update = [
                        'shop'         => $offer['_embedded']['shop']['id'],
                        'productOffer' => [
                            'shop' => $offer['_embedded']['shop']['id'],
                            'id'   => $offer['id'],
                        ],
                    ];

                    $returns = $result['_embedded']['returns'];
                    $return = $returns[0];

                    $result = $api->setRequest($update)->request(
                        Api::TYPE_PATCH,
                        $return['_links']['self']['href'],
                        [],
                        false
                    )->getResult();

                    $helper->log(
                        sprintf(
                            'Return %s offer was updated',
                            $result['id']
                        )
                    );

                }

                $helper->log(
                    sprintf(
                        'Return "%s" exists', $returnData['extId']
                    )
                );
            }
        }
    }
}
