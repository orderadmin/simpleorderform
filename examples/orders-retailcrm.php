<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$totalPages = 1;
for ($page = 1; $page <= $totalPages; $page++) {
    $url = sprintf(
        'https://%s/api/v5/orders?%s',
        $config['crm-url'], http_build_query([
            'apiKey'         => $config['api-key'],
            'page'           => $page,
            'limit'          => 100,
            'filter'         => [
                'extendedStatus' => [
                    'assembling-complete', 'shipped',
                ],
            ],
            'sites'          => [
                'retailcrm3-qweqweqweqwe-ru',
            ],
            'shipmentStores' => [
                1,
            ],
        ])
    );

    $orders = file_get_contents($url);
    $orders = json_decode($orders, true);

    $totalPages = $orders['pagination']['totalPageCount'];

    foreach ($orders['orders'] as $order) {
        $helper->log(sprintf('Order is %s', $order['number']));

        $result = $api->request(
            Api::TYPE_GET, '/api/products/order', [
                'search'   => $order['number'],
                'filter'   => [
                    [
                        'field' => 'shop',
                        'type'  => 'eq',
                        'value' => 173512,
                    ],
                ],
                'fields'   => [
                    'id', 'state', 'shipmentDate', 'deliveryRequest',
                ],
                'per_page' => 250,
                'order-by' => [
                    [
                        'type'      => 'field',
                        'field'     => 'shipmentDate',
                        'direction' => 'asc',
                    ],
                ],
            ]
        )->getResult();

        $helper->log(sprintf('Found %s orders', $result['total_items']));
        if ($result['total_items'] > 1) {
            $helper->log(sprintf('Need to find exact order'));

            continue;
        }

        $oaOrders = $result['_embedded']['order'];
        $oaOrder = $oaOrders[0];

        if ($oaOrder['state'] == 'assembled') {
            $helper->log(sprintf('The order have correct state'));

            continue;
        }

        $patchUrl = parse_url($oaOrder['_links']['self']['href'], PHP_URL_PATH);
        $helper->log(sprintf('Patch URL: %s', $patchUrl));

        $oaOrder = $api->setRequest(
            [
                'state' => $oaOrder['state']
            ]
        )->request(Api::TYPE_PATCH, $patchUrl)->getResult();

        $deliveryRequest = $oaOrder['_embedded']['deliveryRequest'];

        $patchUrl = parse_url(
            $deliveryRequest['_links']['self']['href'], PHP_URL_PATH
        );
        $helper->log(sprintf('Patch URL: %s', $patchUrl));

        $deliveryRequest = $api->setRequest(
            [
                'state' => $deliveryRequest['state'],
            ]
        )->request(Api::TYPE_PATCH, $patchUrl)->getResult();
    }
}
