<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$pageCount = 1;
for ($page = 1; $page <= $pageCount; $page++) {
    $documentItemsResult = $api->request(
        Api::TYPE_GET, '/api/storage/movements/document/item', [
            'filter'   => [
                [
                    'type'  => 'eq',
                    'field' => 'warehouse',
                    'value' => 13396,
                ],
                [
                    'type'  => 'isNotNull',
                    'field' => 'productOffer',
                ],
                [
                    'type'  => 'eq',
                    'field' => 'status',
                    'value' => 'blocked',
                ],
            ],
            'page'     => $page,
            'per_page' => 250,
        ]
    )->getResult();

    $pageCount = $documentItemsResult['page_count'];

    $helper->log(
        sprintf(
            'Page number %s, page count %s, items %s', $page, $pageCount,
            $documentItemsResult['total_items']
        )
    );

    foreach (
        $documentItemsResult['_embedded']['document_item_id'] as $documentItem
    ) {
        $helper->log(
            sprintf(
                'Document item %s, document %s', $documentItem['id'],
                $documentItem['_embedded']['document']['id']
            )
        );
        // echo json_encode($documentItem, JSON_PRETTY_PRINT);

        $patchUrl = parse_url(
            $documentItem['_links']['self']['href'], PHP_URL_PATH
        );

        $helper->log(sprintf('Patch URL: %s', $patchUrl));

        $res = $api->setRequest([
            'status' => 'normal',
        ])->request(Api::TYPE_PATCH, $patchUrl)->getResult();
    }
}
