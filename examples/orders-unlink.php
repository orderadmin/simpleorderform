<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$allPages = 1;
for ($page = 1; $page <= $allPages; $page++) {
    $result = $api->request(
        Api::TYPE_GET, '/api/products/order', [
            'filter'   => [
                [
                    'field' => 'source',
                    'type'  => 'eq',
                    'value' => 7869,
                ],
                [
                    'field' => 'state',
                    'type'  => 'in',
                    'values' => ['partly_reserved', 'cancel', 'assembling',],
                ],
                //            [
                //                'field'  => 'id',
                //                'type'   => 'notIn',
                //                'values' => $protected,
                //            ],
            ],
            'fields'   => [
                'id', 'state', 'shipmentDate', 'created',
            ],
            'page' => $page,
            'per_page' => 250,
            'order-by' => [
                [
                    'type'      => 'field',
                    'field'     => 'shipmentDate',
                    'direction' => 'asc',
                ],
            ],
        ]
    )->getResult();

//$db = new \SQLite3('./examples/dbase/data-offers.db');
//$db->exec('PRAGMA journal_mode = wal;');

    $helper->log(sprintf('Found %s orders, %s pages', $result['total_items'], $result['page_count']));

    $allPages = $result['page_count'];

    $orders = $result['_embedded']['order'];
    foreach ($orders as $order) {
        if ($order['type'] !== 'retail') {
            $helper->log(
                sprintf(
                    'Order %s: %s, %s', $order['id'], $order['type'],
                    $order['state']
                )
            );
        }

        if (!in_array($order['state'], [
            'pending', 'pending_error', 'partly_reserved', 'cancel', 'assembling',
        ])
        ) {
            $helper->log(
                sprintf('Order %s: %s', $order['id'], $order['state'])
            );

            continue;
        }

//    $helper->log(var_export($order, true));

        if ($order['state'] === 'cancel') {
            $newState = 'deleted';
        } else {
            $newState = 'cancel';
        }

        try {
            $result = $api->setRequest(
                [
                    'state' => $newState
                ]
            )->request(
                Api::TYPE_PATCH,
                $order['_links']['self']['href']
            )->getResult();

            if ($result['state'] == $newState) {
                $helper->log(
                    sprintf('Order %s: %s', $order['id'], $newState)
                );
            }
        } catch (\Exception $e) {
            $helper->log($e->getMessage());
        }
    }
}