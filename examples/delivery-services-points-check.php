<?php

namespace App;

use App\Lib\Api;
use Geocoder\Location;
use Geocoder\Query\GeocodeQuery;
use Http\Client\Curl\Client;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);
$cache->getOptions()->setNamespace(
    preg_replace("/[^A-Za-z0-9 ]/", '', $config['user'])
);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$result = $api->request(
    Api::TYPE_GET, '/api/users/users/0'
)->getResult();

$helper->log(sprintf('Working user: %s', $result['id']));

$startPage = $totalPages = $argv[1] ?? 1;

for ($page = $startPage; $page <= $totalPages; $page++) {
    $result = $api->request(
        Api::TYPE_GET, '/api/delivery-services/service-points', [
        'filter'   => [
            [
                'field' => 'geo',
                'type'  => 'isnull',
            ],
        ],
        'per_page' => 250,
        'page'     => $page,
    ], false
    )->getResult();

    if ($totalPages != $result['page_count']) {
        $totalPages = $result['page_count'];
    }

    $httpClient = new Client();
    $provider = new \Geocoder\Provider\GoogleMaps\GoogleMaps(
        $httpClient, null, $config['google-api-key']
    );
    $geocoder = new \Geocoder\StatefulGeocoder($provider, 'en');

    $i = 1;
    foreach ($result['_embedded']['servicePoints'] as $servicePoint) {
        $helper->log(
            sprintf(
                'Updating service point: %s ', $servicePoint['name']
            )
        );

        $search = sprintf('%s', $servicePoint['rawAddress']);

        if (empty($servicePoint['rawAddress'])) {
            $search = sprintf(
                '%s %s', $servicePoint['name'], $servicePoint['extId']
            );
        }

        $geo = $geocoder->geocodeQuery(GeocodeQuery::create($search));

        if (count($geo->getIterator()) > 1) {
            $helper->log(sprintf('Several matches found'));

            continue;
        }

        /** @var Location $location */
        foreach ($geo->getIterator() as $location) {
            $helper->log(
                sprintf(
                    'Found match: %s (id %d)', $servicePoint['name'],
                    $servicePoint['id']
                )
            );

            $updateData = [];
            if (empty($servicePoint['geo'])) {
                $updateData['geo'] = [
                    'latitude'  => $location->getCoordinates()
                        ->getLatitude(),
                    'longitude' => $location->getCoordinates()
                        ->getLongitude(),
                ];
            }

            if (!empty($updateData)) {
                $res = $api->setRequest($updateData)->request(
                    Api::TYPE_PATCH, sprintf(
                        '/api/delivery-services/service-points/%s',
                        $servicePoint['id']
                    )
                )->getResult();

                $helper->log(
                    sprintf(
                        'Service point %s was updated: %s', $res['id'],
                        var_export($updateData, true)
                    )
                );
            }
        }
    }
}
