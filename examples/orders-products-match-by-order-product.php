<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$result = $api->request(
    Api::TYPE_GET, '/api/products/order', [
        'filter'   => [
            [
                'field' => 'source',
                'type'  => 'eq',
                'value' => 9032,
            ],
            [
                'field'  => 'state',
                'type'   => 'in',
                'values' => ['partly_reserved',],
            ],
        ],
        'fields'   => [
            'id', 'state', 'shipmentDate',
        ],
        'page'     => 3,
        'per_page' => 250,
        'order-by' => [
            [
                'type'      => 'field',
                'field'     => 'shipmentDate',
                'direction' => 'asc',
            ],
        ],
    ]
)->getResult();

$helper->log(sprintf('Found %s orders', $result['total_items']));

$orders = $result['_embedded']['order'];

$i = 0;
foreach ($orders as $order) {
    $orderId = trim($order['id']);

    $result = $api->request(
        Api::TYPE_GET, '/api/products/order/product', [
            'filter'   => [
                [
                    'field' => 'order',
                    'type'  => 'eq',
                    'value' => $orderId,
                ],
            ],
            'per_page' => 250,
        ]
    )->getResult();

    $helper->log(
        sprintf(
            'Found %s order products for order %s', $result['total_items'],
            $orderId
        )
    );

    if (empty($result['total_items'])) {
        continue;
    }

    $orderProducts = $result['_embedded']['order_product'];
    foreach ($orderProducts as $orderProduct) {
//        $helper->log(var_export($orderProduct, true));

        $offer = $orderProduct['_embedded']['productOffer'];

        if (empty($offer['eav']['integrations-amazon-offer-sku-fbm'])
            && !empty($offer['eav']['integrations-amazon-offer-sku-fbm-list'])
        ) {
            $helper->log(var_export($offer, true));

            $eav = $offer['eav'];
            $eav['integrations-amazon-offer-sku-fbm']['ATVPDKIKX0DER']
                = $offer['eav']['integrations-amazon-offer-sku-fbm-list']['ATVPDKIKX0DER'][0];

            try {
                $offer = $api->setRequest(
                    [
                        'eav' => $eav,
                    ]
                )->request(
                    Api::TYPE_PATCH,
                    sprintf(
                        '/api/products/offer/%s/%s',
                        $offer['_embedded']['shop']['id'],
                        $offer['id']
                    )
                )->getResult();
            } catch (\Exception $e) {
                $helper->log($e->getMessage());
            }

            $helper->log(
                sprintf(
                    'Updated offer %s',
                    $offer['id']
                )
            );
        }

        $fbmSku
            = $offer['eav']['integrations-amazon-offer-sku-fbm']['ATVPDKIKX0DER']['9032'];

        $result = $api->request(
            Api::TYPE_GET, '/api/storage/movements/document/item', [
                'filter'   => [
                    [
                        'type'   => 'in',
                        'field'  => 'sku',
                        'values' => array_merge([
                            $fbmSku,
                        ], $offer['barcodes']),
                    ],
                ],
                'per_page' => 250,
            ]
        )->getResult();

        //123281
//        echo json_encode($result, JSON_PRETTY_PRINT);
        $helper->log(sprintf('Found %s items', $result['total_items']));
        if ($result['total_items'] > 0) {
            foreach ($result['_embedded']['document_item_id'] as $documentItem)
            {
                $patchUrl = parse_url(
                    $documentItem['_links']['self']['href'], PHP_URL_PATH
                );

                $helper->log(sprintf('Patch URL: %s', $patchUrl));

                if (empty($documentItem['_embedded']['productOffer'])) {
                    $helper->log(sprintf('Patching product offer'));

                    $res = $api->setRequest([
                        'productOffer' => [
                            'shop' => $offer['_embedded']['shop']['id'],
                            'id'   => $offer['id'],
                        ],
                        'status'       => 'normal',
                    ])->request(Api::TYPE_PATCH, $patchUrl)->getResult();
                } else {
                    $helper->log(sprintf('Unblocking document item'));

                    $res = $api->setRequest([
                        'status' => 'normal',
                    ])->request(Api::TYPE_PATCH, $patchUrl)->getResult();
                }

//                echo json_encode($documentItem, JSON_PRETTY_PRINT);
//                die();
            }
        }

        $newState = 'pending_queued';

        $order = $api->setRequest(
            [
                'state' => $newState
            ]
        )->request(
            Api::TYPE_PATCH,
            sprintf('/api/products/order/%s', $orderId)
        )->getResult();

        if ($order['state'] == $newState) {
            $helper->log(
                sprintf('Order %s: %s', $order['id'], $newState)
            );
        }

        if ($i++ === 250) {
            die();
        }
    }
}