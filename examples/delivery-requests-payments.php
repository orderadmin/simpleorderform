<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$result = $api->request(
    Api::TYPE_GET, '/api/delivery-services/requests', [
        'filter'   => [
            [
                'field' => 'source',
                'type'  => 'eq',
                'value' => 8311,
            ],
            [
                'field' => 'payment',
                'type'  => 'gt',
                'value' => 0,
            ],
        ],
        'per_page' => 250,
        'fields'   => [
            'id',
            'payment',
            'paymentState',
            'rawData',
        ],
    ]
)->getResult();

$helper->log(sprintf('Found %s delivery requests', $result['total_items']));

$deliveryRequests = $result['_embedded']['delivery_requests'];
foreach ($deliveryRequests as $deliveryRequest) {
    if (empty($deliveryRequest['rawData'])
        || $deliveryRequest['rawData']['delivery']['cod'] > 0
    ) {
        continue;
    }

//    $helper->log(var_export($deliveryRequest, true));

    $newState = 'missing';
    $backState = null;
    if ($deliveryRequest['paymentState'] !== 'payment_waiting') {
        $backState = $deliveryRequest['paymentState'];
    }

    $patchUrl = parse_url(
        $deliveryRequest['_links']['self']['href'], PHP_URL_PATH
    );
    $helper->log(sprintf('Patch URL: %s', $patchUrl));

    $result = $api->setRequest(
        [
            'paymentState' => $newState
        ]
    )->request(Api::TYPE_PATCH, $patchUrl)->getResult();

    if ($result['paymentState'] == $newState) {
        $helper->log(
            sprintf(
                'Delivery request %s: %s', $deliveryRequest['id'], $newState
            )
        );
    }

    if (!empty($backState)) {
        $result = $api->setRequest(
            [
                'paymentState' => $backState
            ]
        )->request(Api::TYPE_PATCH, $patchUrl)->getResult();

        if ($result['paymentState'] == $backState) {
            $helper->log(
                sprintf(
                    'Delivery request %s return payment state is: %s',
                    $deliveryRequest['id'], $backState
                )
            );
        }
    }
}