<?php

namespace App;

use App\Lib\Api;
use GraphQL\Client;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use function Symfony\Component\String\s;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$client = new Client(
    'https://public-api.shiphero.com/graphql',
    ['Authorization' => 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlJUQXlOVU13T0Rrd09ETXhSVVZDUXpBNU5rSkVOVVUxUmtNeU1URTRNMEkzTWpnd05ERkdNdyJ9.eyJodHRwOi8vc2hpcGhlcm8tcHVibGljLWFwaS91c2VyaW5mbyI6eyJuYW1lIjoiU2VyZ2V5IEFkdXNoZWV2IiwiZmlyc3RfbmFtZSI6IlNlcmdleSIsImxhc3RfbmFtZSI6IkFkdXNoZWV2Iiwibmlja25hbWUiOiJzdGFmZiIsInBpY3R1cmUiOiJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci8yMTUwOWIwZmNmZDhiMmFkZDUyMmJlYjcyNTg3MTgxNz9zPTQ4MCZyPXBnJmQ9aHR0cHMlM0ElMkYlMkZjZG4uYXV0aDAuY29tJTJGYXZhdGFycyUyRnNhLnBuZyIsImFjY291bnRfaWQiOjU5Mzg0LCJpc19hY2NvdW50X2FkbWluIjpmYWxzZX0sImlzcyI6Imh0dHBzOi8vbG9naW4uc2hpcGhlcm8uY29tLyIsInN1YiI6ImF1dGgwfDVmYzdiZDE0ZWRhODZhMDA2OTY3ZGUwMiIsImF1ZCI6WyJzaGlwaGVyby1wdWJsaWMtYXBpIiwiaHR0cHM6Ly9zaGlwaGVyby5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNjk4ODQ3NDQzLCJleHAiOjE3MDEyNjY2NDMsImF6cCI6Im10Y2J3cUkycjYxM0RjT04zRGJVYUhMcVF6UTRka2huIiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSB2aWV3OnByb2R1Y3RzIGNoYW5nZTpwcm9kdWN0cyB2aWV3Om9yZGVycyBjaGFuZ2U6b3JkZXJzIHZpZXc6cHVyY2hhc2Vfb3JkZXJzIGNoYW5nZTpwdXJjaGFzZV9vcmRlcnMgdmlldzpzaGlwbWVudHMgY2hhbmdlOnNoaXBtZW50cyB2aWV3OnJldHVybnMgY2hhbmdlOnJldHVybnMgdmlldzp3YXJlaG91c2VfcHJvZHVjdHMgY2hhbmdlOndhcmVob3VzZV9wcm9kdWN0cyB2aWV3OnBpY2tpbmdfc3RhdHMgdmlldzpwYWNraW5nX3N0YXRzIG9mZmxpbmVfYWNjZXNzIiwiZ3R5IjoicGFzc3dvcmQifQ.E5YTLMR4OHQph-68jbPzXI50ujFJdAH-Pq0Ht7tTwwFgpA0qtWTS-CAPY3mGElMWo0dRAfmJRSGMJ6eKolhAppuU1uCAJJ59nEv2H_1dc_Jbt-tfITl2P4Kk3T5RI40CLfklGhJEBgY2EyWx0dgNtpv6uxUmom18YF7NTMxX7XA1NkfxhhNyPrReoTvxopJ8Ae5T1cZpCrPZOu1e1bfrMobzuyX271kBYcJ0hu447xpsXGeGfYz15_5I5gum5xpo572D5LL_uzR-fL0l5mY38WVsWFE3sp-7sOjisaD4b-0xBbMheqiO_HsjnjzmnutMt5viCiFDbtJgDG9WFG-5vg']
);

$itemsData = file_get_contents($config['shiphero-snapshot']);
$itemsData = json_decode($itemsData, JSON_PRETTY_PRINT);
$itemsData = $itemsData['products'];

$cnt = 0;
$skip = 0;// MFR0448682USBCDbox1HOUSE
foreach ($itemsData as $sku => $itemData) {
    if (!in_array($sku, ['200420490INSK'])) {
        continue;
    }

    if ($cnt++ < $skip) {
        continue;
    }

    $helper->log(sprintf('Item number "%s"', $cnt));

    foreach ($itemData['warehouse_products'] as $wProduct) {
        $noQuantity = true;
        foreach ($wProduct['item_bins'] as $itemBin) {
            if ($itemBin['location_name'] !== 'Receiving') {
                if ($itemBin['quantity'] > 0) {
                    $noQuantity = false;
                } else {
//                    echo json_encode($itemData, JSON_PRETTY_PRINT);
//
//                    die();
                }
            }
        }

        if ($noQuantity) {
            continue 2;
        }
    }

    $helper->log(sprintf('Loading SKU "%s"', $itemData['sku']));

//    $helper->log(json_encode($itemData, JSON_PRETTY_PRINT));

    $offer = [
        'shop' => 202973,
        'type' => 'simple',
        'sku'  => $itemData['sku'],
    ];

    $result = $api->request(
        Api::TYPE_GET, '/api/products/offer', [
            'filter'   => [
                [
                    'field' => 'shop',
                    'type'  => 'eq',
                    'value' => $offer['shop'],
                ],
                //                [
                //                    'field' => 'sku',
                //                    'type'  => 'eq',
                //                    'value' => $offer['sku'],
                //                ],
            ],
            'search'   => $offer['sku'],
            'fields'   => [
                'id', 'shop', 'sku', 'article', 'barcodes',
            ],
            'per_page' => 250,
        ]
    )->getResult();

    $offersData = [];
    $helper->log(sprintf('Found %s offers', $result['total_items']));
    if (empty($result['total_items']) || $result['total_items'] > 1) {
        $perPage = 100;

        $sku = $offer['sku'];

        $fromFiles = false;
        if ($fromFiles) {
            $i = 1;
            while ($i) {
                $helper->log(
                    sprintf(
                        'Searching file: %s',
                        sprintf('%s/sh/offers_%s.json', __DIR__, $i)
                    )
                );

                if (!file_exists(
                    sprintf('%s/sh/offers_%s.json', __DIR__, $i)
                )
                ) {
                    break;
                }

                $res = file_get_contents(
                    sprintf('%s/sh/offers_%s.json', __DIR__, $i)
                );

                if (empty($res)) {
                    break;
                }

                $i++;

                $res = json_decode($res, true);
                foreach ($res as $offerData) {
                    $offerData = $offerData['node'];

                    if ($offerData['sku'] === $sku) {
                        $offersData = [
                            [
                                'node' => $offerData,
                            ],
                        ];

                        break;
                    }
                }

                if ($i === 500) {
                    break;
                }

                if (!empty($offersData)) {
                    break;
                }
            }
        }

        $fromApi = true;
        if (empty($offersData) && $fromApi) {
            $gql = <<<QUERY
query {
  products(sku: "$sku") {
    request_id
    complexity
    data(first: $perPage) {
      edges {
        node {
          id
          legacy_id
          account_id
          name
          sku
          barcode
          country_of_manufacture
          dimensions {
            height
            width
            length
            weight
          }
          tariff_code
          kit
          kit_build
          no_air
          final_sale
          customs_value
          customs_description
          not_owned
          dropship
          needs_serial_number
          thumbnail
          large_thumbnail
          created_at
          updated_at
          product_note
          virtual
          ignore_on_invoice
          ignore_on_customs
          active
          warehouse_products {
            warehouse_id
            on_hand
          }
          images {
            src
          }
          tags
          kit_components {
            sku
            quantity
          }
        }
      }
    }
  }
}
QUERY;

            $res = $client->runRawQuery($gql);

            $offersData = $res->getData();
            $offersData = json_decode(json_encode($offersData), true);
            $offersData = $offersData['products']['data']['edges'];
        }

        if (empty($offersData)) {
            $helper->log(sprintf('No offers found'));

            continue;
        }

        foreach ($offersData as $offerData) {
//        echo json_encode($offerData, JSON_PRETTY_PRINT);

            $offerData = $offerData['node'];

            $offer = [
                'shop'     => $config['shop'],
                'type'     => 'simple',
                'extId'    => $offerData['id'],
                'name'     => $offerData['name'],
                'image'    => $offerData['large_thumbnail'],
                'sku'      => $offerData['sku'],
                'barcodes' => [
                    $offerData['barcode'],
                ],
                'raw'      => $offerData,
            ];

            try {
                $result = $api->request(
                    Api::TYPE_GET, '/api/products/offer', [
                        'filter'   => [
                            [
                                'field' => 'shop',
                                'type'  => 'eq',
                                'value' => $offer['shop'],
                            ],
                            [
                                'field' => 'sku',
                                'type'  => 'eq',
                                'value' => $offer['sku'],
                            ],
                        ],
                        'fields'   => [
                            'id', 'shop', 'sku', 'article',
                        ],
                        'per_page' => 250,
                    ]
                )->getResult();

                $helper->log(
                    sprintf('Found %s offers', $result['total_items'])
                );
                if (empty($result['total_items'])) {
                    $result = $api->setRequest($offer)->request(
                        Api::TYPE_POST, '/api/products/offer', [
                            'filter'   => [
                                [
                                    'field' => 'shop',
                                    'type'  => 'eq',
                                    'value' => $offer['shop'],
                                ],
                                [
                                    'field' => 'sku',
                                    'type'  => 'eq',
                                    'value' => $offer['sku'],
                                ],
                            ],
                            'fields'   => [
                                'id', 'shop', 'sku', 'article',
                            ],
                            'per_page' => 250,
                        ]
                    )->getResult();

                    $helper->log(
                        sprintf('Added offer %s', $result['id'])
                    );

                    $offer = $result;
                }
            } catch (\Exception $e) {
                $helper->log($e->getMessage());

                continue;
            }
        }
//
//        echo json_encode($itemData, JSON_PRETTY_PRINT);
//        die();
    }

    if (!empty($result['_embedded']['product_offer'][0])) {
        $offer = $result['_embedded']['product_offer'][0] ?? null;
    }

    if (empty($offer)) {
        die();
    }

    foreach ($itemData['warehouse_products'] as $wProduct) {
        foreach ($wProduct['item_bins'] as $itemBin) {
            if ($itemBin['quantity'] > 0 && !empty($itemBin['location_name'])) {
                $place = null;
                $result = $api->request(
                    Api::TYPE_GET, '/api/storage/place', [
                        'filter'   => [
                            [
                                'field' => 'extId',
                                'type'  => 'eq',
                                'value' => $itemBin['location_name'],
                            ],
                        ],
                        'per_page' => 150,
                    ]
                )->getResult();

                if (empty($result['total_items'])
                    || $result['total_items'] > 1
                ) {
                    $helper->log(
                        sprintf(
                            'Found %s locations, name "%s"',
                            $result['total_items'],
                            $itemBin['location_name']
                        )
                    );

                    if ($itemBin['location_name'] !== 'Receiving') {
                        file_put_contents(
                            '/home/oadev/domains/s.orderadmin.eu/public_html/forms/examples/sh/places.txt',
                            $itemBin['location_name'] . "\n",
                            FILE_APPEND
                        );
                    }

                    continue 3;
                }

                $place = $result['_embedded']['place'][0];

                $itemsFilter = [
                    [
                        'field' => 'place',
                        'type'  => 'eq',
                        'value' => $place['id'],
                    ],
                    [
                        'field' => 'productOffer',
                        'type'  => 'eq',
                        'value' => $offer['id'],
                    ],
                ];

                if (empty($place['id']) || empty($offer['id'])) {
                    var_dump($itemsFilter);
                    die();
                }

                $result = $api->request(
                    Api::TYPE_GET, '/api/storage/item', [
                        'filter'   => $itemsFilter,
                        'group'    => [
                            [
                                'alias' => 'i',
                                'field' => 'place',
                            ],
                            [
                                'alias' => 'i',
                                'field' => 'productOffer',
                            ],
                        ],
                        'per_page' => 150,
                    ]
                )->getResult();

                if ($result['total_items'] > 1) {
                    $helper->log(
                        sprintf(
                            'Found %s items',
                            $result['total_items']
                        )
                    );

                    die();
                }

                $itemsOnLocation = $result['_embedded']['item'];
                $itemOnLocation = $itemsOnLocation[0] ?? null;
                if (!empty($itemOnLocation)
                    && $itemOnLocation['count'] === $itemBin['quantity']
                ) {
                    $helper->log(
                        sprintf(
                            'Quantity at the location is %s (same as in the bin)',
                            $itemOnLocation['count']
                        )
                    );

                    continue 3;
                } else {
                    $helper->log(
                        sprintf(
                            'Quantity at the location is %s (%s in the bin)',
                            $itemOnLocation['count'] ?? 0,
                            $itemBin['quantity'] ?? 0
                        )
                    );
                }

                if (empty($itemOnLocation['count'])) {
                    $itemOnLocation['count'] = 0;
                } elseif ($itemOnLocation['count'] > 1) {
                    $helper->log(sprintf('Skipping items location'));

                    continue;
                }

                if (empty($itemOnLocation['count'])) {
                    $sItemData = [
                        'warehouse'    => $config['warehouse'],
                        'productOffer' => [
                            'id'   => $offer['id'],
                            'shop' => $config['shop'],
                        ],
                        'state'        => 'normal',
                        'place'        => $place['id'],
                        'sku'          => $offer['barcodes'][0] ??
                            $offer['sku'],
                        'comment'      => json_encode($itemData),
                    ];

                    if (empty($offer['sku'])) {
                        $sItemData['sku'] = sprintf('S/I/PO%s*', $offer['id']);
                    }

                    $result = $api->setRequest($sItemData)->request(
                        Api::TYPE_POST, '/api/storage/item'
                    )->getResult();

                    $helper->log(
                        sprintf('Created item %s (%s)', $result['id'], $cnt)
                    );

                    $itemOnLocation['count']++;
                }

                $bulk = 1;
                $quantityToChange = ($itemBin['quantity']
                    - $itemOnLocation['count']);
                if ($quantityToChange > 0) {
                    if ($bulk) {
                        $helper->log(
                            json_encode([
                                'filter'   => $itemsFilter,
                                'per_page' => $itemBin['quantity'],
                            ], JSON_PRETTY_PRINT)
                        );

                        $result = $api->setRequest([])->request(
                            Api::TYPE_PUT, '/api/storage/item/bulk', [
                                'filter'   => $itemsFilter,
                                'per_page' => $itemBin['quantity'],
                            ]
                        )->getResult();

                        $helper->log(
                            sprintf(
                                'Final quantity adjusted (added %s)',
                                $quantityToChange
                            )
                        );

//                    $helper->log(
//                        sprintf('Final quantity is %s (%s)', $result['_embedded']['items']['count'], $cnt)
//                    );
                    } else {
                        for (
                            $quantity = $itemOnLocation['count'];
                            $quantity < $itemBin['quantity'];
                            $quantity++
                        ) {
                            $sItemData = [
                                'warehouse'    => $config['warehouse'],
                                'productOffer' => [
                                    'id'   => $offer['id'],
                                    'shop' => $config['shop'],
                                ],
                                'state'        => 'normal',
                                'place'        => $place['id'],
                                'sku'          => $offer['barcodes'][0] ??
                                    $offer['sku'],
                                'comment'      => json_encode($itemData),
                            ];

                            if (empty($offer['sku'])) {
                                $sItemData['sku'] = sprintf(
                                    'S/I/PO%s*', $offer['id']
                                );
                            }

                            $result = $api->setRequest($sItemData)->request(
                                Api::TYPE_POST, '/api/storage/item'
                            )->getResult();

                            $helper->log(
                                sprintf(
                                    'Created item %s (%s)', $result['id'], $cnt
                                )
                            );
                        }
                    }
                }
            }
        }
    }
//
//    echo json_encode($result, JSON_PRETTY_PRINT);
//    echo json_encode($itemData, JSON_PRETTY_PRINT);
//    die();
}

//$db = new \SQLite3('./examples/dbase/data-offers.db');
//$db->exec('PRAGMA journal_mode = wal;');

//$res = file_get_contents($config['shiphero-snapshot']);
//$res = json_decode($res, true);
//
//echo json_encode($res, JSON_PRETTY_PRINT);
//
//die();
//
//$totalPages = 1;
//for ($page = 1; $page <= $totalPages; $page++) {
//    $result = $api->request(
//        Api::TYPE_GET, '/api/products/offer', [
//            'filter'   => [
//                [
//                    'field' => 'shop',
//                    'type'  => 'eq',
//                    'value' => 177307,
//                ],
//                [
//                    'field' => 'name',
//                    'type'  => 'isNull',
//                ],
//            ],
//            'fields'   => [
//                'id', 'shop', 'sku', 'article',
//            ],
//            'page'     => $page,
//            'per_page' => 250,
//            'order-by' => [
//                [
//                    'type'      => 'field',
//                    'field'     => 'id',
//                    'direction' => 'asc',
//                ],
//            ],
//        ]
//    )->getResult();
//
//    $helper->log(sprintf('Found %s offers', $result['total_items']));
//
//    if ($page == 1) {
//        $totalPages = $result['page_count'];
//
//        $helper->log(sprintf('Found %s pages', $totalPages));
//    }
//
//    $productOffers = $result['_embedded']['product_offer'];
//    foreach ($productOffers as $productOffer) {
////    $helper->log(var_export($productOffer, true));
//
//        $productOffer = $api->request(
//            Api::TYPE_GET, sprintf(
//                '/api/products/offer/%s/%s',
//                $productOffer['_embedded']['shop']['id'], $productOffer['id']
//            )
//        )->getResult();
//
//        if (empty($productOffer['eav']['integrations-amazon-offer-asin'])) {
//            $helper->log(
//                sprintf('Product offer %s, have no ASIN', $productOffer['id'])
//            );
//
//            continue;
//        }
//
//        try {
//            $result = $api->setRequest(
//                [
//                    'eav' => [
//                        'integrations-amazon-offer-asin' => [
//                            'ATVPDKIKX0DER' => $productOffer['eav']['integrations-amazon-offer-asin']['ATVPDKIKX0DER'],
//                        ],
//                    ],
//                ]
//            )->request(
//                Api::TYPE_PATCH,
//                sprintf(
//                    '/api/products/offer/%s/%s',
//                    $productOffer['_embedded']['shop']['id'],
//                    $productOffer['id']
//                )
//            )->getResult();
//        } catch (\Exception $e) {
//            $helper->log($e->getMessage());
//        }
//
//        $helper->log(
//            sprintf(
//                'Updated product offer %s, ASIN: %s (page %s)',
//                $productOffer['id'],
//                $result['eav']['integrations-amazon-offer-asin']['ATVPDKIKX0DER'], $page
//            )
//        );
//    }
//}
