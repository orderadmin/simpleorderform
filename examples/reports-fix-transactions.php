<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);
$cache->getOptions()->setNamespace(
    preg_replace("/[^A-Za-z0-9 ]/", '', $config['user'])
);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id' => $config['client'],
                'domain' => $config['domain'],
                'grant_type' => 'password',
                'username' => $config['user'],
                'password' => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$result = $api->request(
    Api::TYPE_GET, '/api/users/users/0'
)->getResult();

$helper->log(sprintf('Working user: %s', $result['id']));

$reportId = 0;
$helper->log(sprintf('Working report: %s', $reportId));

$report = $api->request(
    Api::TYPE_GET, sprintf('/api/reports/report/%s', $reportId)
)->getResult();

$entries = array_reverse($report['result']['entries']);
foreach ($entries as $key => $entry) {
    $helper->log(sprintf('Working entry: %s', $key));
    $transactions = $entry['transactions'];

    foreach ($transactions as $data) {
        $helper->log(sprintf('Working transaction: %s', $data['id']));

        $transaction = $api->request(
            Api::TYPE_GET, sprintf('/api/accounts/transaction/%s', $data['id'])
        )->getResult();

        $transactionDate = new \DateTime(
            $transaction['transactionDate']['date']
        );

        if ($data['date'] <> $transactionDate->format('d.m.Y')) {
            $helper->log(
                sprintf(
                    'Report date: %s, transaction: %s', $data['date'],
                    $transaction['transactionDate']['date']
                )
            );

            $updateData = [
                'transactionDate' => $data['date'],
            ];

            $res = $api->setRequest($updateData)->request(
                Api::TYPE_PATCH,
                sprintf('/api/accounts/transaction/%s', $data['id'])
            )->getResult();

            $helper->log(
                sprintf(
                    'Transaction %s was updated: %s', $res['id'],
                    var_export($updateData, true)
                )
            );
        }
    }
}
