<?php

namespace App;

use App\Lib\Api;
use Geocoder\Location;
use Geocoder\Model\AdminLevel;
use Geocoder\Query\GeocodeQuery;
use Http\Client\Curl\Client;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);
$cache->getOptions()->setNamespace(
    preg_replace("/[^A-Za-z0-9 ]/", '', $config['user'])
);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$result = $api->request(
    Api::TYPE_GET, '/api/users/users/0'
)->getResult();

$helper->log(sprintf('Working user: %s', $result['id']));

$result = $api->request(
    Api::TYPE_GET, '/api/delivery-services/postcodes', [
    'filter'   => [
        [
            'field' => 'country',
            'type'  => 'eq',
            'value' => 129,
        ],
        [
            'field' => 'locality',
            'type'  => 'isnull',
        ],
    ],
    'per_page' => 250,
], false
)->getResult();

$httpClient = new Client();
$provider = new \Geocoder\Provider\GoogleMaps\GoogleMaps(
    $httpClient, null, $config['google-api-key']
);
$geocoder = new \Geocoder\StatefulGeocoder($provider, 'en');

$i = 1;
foreach ($result['_embedded']['postcodes'] as $postcode) {
    $helper->log(sprintf('Matching postcode: %s', $postcode['extId']));

    $geo = $geocoder->geocodeQuery(
        GeocodeQuery::create(
            sprintf(
                '%s, %s', $postcode['extId'],
                $postcode['_embedded']['country']['name']
            )
        )
    );

    /** @var Location $location */
    foreach ($geo->getIterator() as $location) {
        /** @var AdminLevel $adminLevel */
        foreach ($location->getAdminLevels() as $adminLevel) {
            $helper->log(
                sprintf('Checking locality: %s', $adminLevel->getName())
            );

            $localityResult = $api->request(
                Api::TYPE_GET, '/api/locations/localities', [
                'search' => $adminLevel->getName(),
                'filter' => [
                    [
                        'field' => 'country',
                        'type'  => 'eq',
                        'value' => 129,
                    ],
                ],
            ], false
            )->getResult();

            if ($localityResult['total_items'] > 0) {
                foreach (
                    $localityResult['_embedded']['localities'] as $locality
                ) {
                    $helper->log(
                        sprintf(
                            'Found match: %s (id %d)', $locality['name'],
                            $locality['id']
                        )
                    );

                    if (empty($locality['geo'])) {
                        $res = $api->setRequest(
                            [
                                'geo' => [
                                    'latitude'  => $location->getCoordinates()
                                        ->getLatitude(),
                                    'longitude' => $location->getCoordinates()
                                        ->getLongitude(),
                                ],
                            ]
                        )->request(
                            Api::TYPE_PATCH, sprintf(
                            '/api/locations/localities/%s', $locality['id']
                        )
                        )->getResult();

                        $helper->log(
                            sprintf(
                                'Locality %s geo was updated: %s', $res['id'],
                                var_export($res['geo'], true)
                            )
                        );
                    }

                    if (1 || $locality['translations']['name_en'] == $adminLevel->getName()) {
                        $res = $api->setRequest(
                            [
                                'locality' => $locality['id'],
                            ]
                        )->request(
                            Api::TYPE_PATCH, sprintf(
                                '/api/delivery-services/postcodes/%s',
                                $postcode['id']
                            )
                        )->getResult();

                        $helper->log(
                            sprintf(
                                'Postcode %s (id %s) locality was updated',
                                $res['extId'],
                                $res['id']
                            )
                        );

                        continue 3;
                    }
                }
            }
        }
    }
}
