<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use function Symfony\Component\String\s;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

//$db = new \SQLite3('./examples/dbase/data-offers.db');
//$db->exec('PRAGMA journal_mode = wal;');

$totalPages = 1;
for ($page = 1; $page <= $totalPages; $page++) {
    $result = $api->request(
        Api::TYPE_GET, '/api/products/offer', [
            'filter'   => [
                [
                    'field' => 'shop',
                    'type'  => 'eq',
                    'value' => 177307,
                ],
                [
                    'field' => 'name',
                    'type'  => 'isNull',
                ],
            ],
            'fields'   => [
                'id', 'shop', 'sku', 'article',
            ],
            'page'     => $page,
            'per_page' => 250,
            'order-by' => [
                [
                    'type'      => 'field',
                    'field'     => 'id',
                    'direction' => 'asc',
                ],
            ],
        ]
    )->getResult();

    $helper->log(sprintf('Found %s offers', $result['total_items']));

    if ($page == 1) {
        $totalPages = $result['page_count'];

        $helper->log(sprintf('Found %s pages', $totalPages));
    }

    $productOffers = $result['_embedded']['product_offer'];
    foreach ($productOffers as $productOffer) {
//    $helper->log(var_export($productOffer, true));

        $productOffer = $api->request(
            Api::TYPE_GET, sprintf(
                '/api/products/offer/%s/%s',
                $productOffer['_embedded']['shop']['id'], $productOffer['id']
            )
        )->getResult();

        if (empty($productOffer['eav']['integrations-amazon-offer-asin'])) {
            $helper->log(
                sprintf('Product offer %s, have no ASIN', $productOffer['id'])
            );

            continue;
        }

        try {
            $result = $api->setRequest(
                [
                    'eav' => [
                        'integrations-amazon-offer-asin' => [
                            'ATVPDKIKX0DER' => $productOffer['eav']['integrations-amazon-offer-asin']['ATVPDKIKX0DER'],
                        ],
                    ],
                ]
            )->request(
                Api::TYPE_PATCH,
                sprintf(
                    '/api/products/offer/%s/%s',
                    $productOffer['_embedded']['shop']['id'],
                    $productOffer['id']
                )
            )->getResult();
        } catch (\Exception $e) {
            $helper->log($e->getMessage());
        }

        $helper->log(
            sprintf(
                'Updated product offer %s, ASIN: %s (page %s)',
                $productOffer['id'],
                $result['eav']['integrations-amazon-offer-asin']['ATVPDKIKX0DER'], $page
            )
        );
    }
}
