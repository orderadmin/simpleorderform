<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\Reader\Xls;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$reader = new Xls();
$spreadsheet = $reader->load('./examples/dev-data/greece-zip-codes.xls');

$helper = new Sample();
foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
    $helper->log('Worksheet - ' . $worksheet->getTitle());

    $dataKeys = [];
    foreach ($worksheet->getRowIterator() as $row) {
        $helper->log('    Row number - ' . $row->getRowIndex());

        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(
            false
        ); // Loop all cells, even if it is not set

        $i = 0;
        $data = [];
        foreach ($cellIterator as $cell) {
            if ($cell !== null) {
                $helper->log(
                    '        Cell - ' . $cell->getCoordinate() . ' - '
                    . $cell->getValue()
                );

                $data[$dataKeys[$i] ?? $i] = $cell->getCalculatedValue();
            }

            $i++;
        }

        if ($row->getRowIndex() == 1) {
            $dataKeys = $data;

            continue;
        }

        if (!empty($data['zipcode'])) {
            $helper->log(var_export($data, true));

            try {
                $result = $api->request(
                    Api::TYPE_GET, '/api/delivery-services/postcodes', [
                    'filter' => [
                        [
                            'field' => 'country',
                            'type'  => 'eq',
                            'value' => 129,
                        ],
                        [
                            'field' => 'extId',
                            'type'  => 'eq',
                            'value' => $data['zipcode'],
                        ],
                    ],
                ], false
                )->getResult();

                if ($result['total_items'] == 0) {
//                    $helper->log(var_export($result, true));
                    $helper->log(var_export($data, true));

                    die();
                } else {
                    $helper->log(sprintf('Postcode %s exists', $data['zipcode']));
                }
            } catch (\Exception $e) {
                echo $e->getMessage() . "\n";

                continue;
            }
        }
    }
}
