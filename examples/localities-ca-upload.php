<?php

namespace App;

use App\Lib\Api;
use Geocoder\Location;
use Geocoder\Query\GeocodeQuery;
use Http\Client\Curl\Client;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);
$cache->getOptions()->setNamespace(
    preg_replace("/[^A-Za-z0-9 ]/", '', $config['user'])
);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$endpoint
    = "https://geoprod.statcan.gc.ca/arcgis/rest/services/Goto_MD2DM_2021_V/MapServer/1/query";

// Set parameters to get the count
$queryParams = [
//    'where' => '1=1',
'where'           => "(SrchType='DPL' OR SrchType='PLACE')",
//  OR SrchType='PCODE'
'returnCountOnly' => 'true',
'f'               => 'json',
];

// Make the request to get the count
$countUrl = $endpoint . '?' . http_build_query($queryParams);

$contextOptions = [
    'http' => [
        'timeout' => 60, // Timeout in seconds
    ],
];

$context = stream_context_create($contextOptions);

$countResponse = file_get_contents($countUrl, false, $context);
$countData = json_decode($countResponse, true);
//$countData = [
//    'count' => 1499722,
//];

$countryId = 83;
// Check if the count request was successful
if ($countData !== null && isset($countData['count'])) {
    $totalRecords = $countData['count'];
    $helper->log("Total Records: $totalRecords");

    // Set parameters for paging
    $resultOffset = 0;
    $resultRecordCount = 100; // Adjust as needed
    unset($queryParams['returnCountOnly']);

    // Iterate through pages until reaching the total count
    while ($resultOffset < $totalRecords) {
        // Set parameters for the current page
        $queryParams = array_merge($queryParams, [
            'outFields'         => '*',
            'f'                 => 'json',
            'resultOffset'      => $resultOffset,
            'resultRecordCount' => $resultRecordCount,
        ]);

        // Make the request for the current page
        $url = $endpoint . '?' . http_build_query($queryParams);
        $response = file_get_contents($url);
        $data = json_decode($response, true);

        // Check if the request for the current page was successful
        if ($data !== null && isset($data['features'])) {
//            $helper->log(json_encode($data, JSON_PRETTY_PRINT));

            foreach ($data['features'] as $dataFeature) {
                $dataFeature = $dataFeature['attributes'];

                $areaName = $dataFeature['EngDispLong'];
                $parts = explode(',', $areaName);
                $areaName = trim(end($parts));

                $filter = [
                    [
                        'field' => 'name',
                        'type'  => 'eq',
                        'value' => $areaName,
                    ],
                    [
                        'field' => 'country',
                        'type'  => 'eq',
                        'value' => $countryId,
                    ],
                ];

                $result = $api->request(
                    Api::TYPE_GET, '/api/locations/area', [
                    'filter'   => $filter,
                    'per_page' => 250,
                    'page'     => 1,
                ], false
                )->getResult();

                if ($result['page_count'] !== 1) {
                    if (empty($result['page_count'])) {
                        $area = $api->setRequest([
                            'state'   => 'active',
                            'extId'   => md5($areaName),
                            'country' => $countryId,
                            'name'    => $areaName,
                        ])->request(
                            Api::TYPE_POST, '/api/locations/area'
                        )->getResult();
                    } else {
                        $helper->log(json_encode($result, JSON_PRETTY_PRINT));

                        die();
                    }
                } else {
                    $area = $result['_embedded']['area'][0];
                }

                $name = trim(explode('/', $dataFeature['MapLabelDisplay'])[0]);

                $localityData = [
                    'name'    => $name,
                    'state'   => 'active',
                    'type'    => $dataFeature['SrchType'],
                    'area'    => $area['id'],
                    'extId'   => $dataFeature['DGuid'],
                    'country' => $countryId,
                    'raw'     => $dataFeature,
                ];

                $filter = [
                    [
                        'field' => 'area',
                        'type'  => 'eq',
                        'value' => $localityData['area'],
                    ],
                    [
                        'field' => 'country',
                        'type'  => 'eq',
                        'value' => $countryId,
                    ],
                    [
                        'field' => 'name',
                        'type'  => 'eq',
                        'value' => $localityData['name'],
                    ],
                ];

                $result = $api->request(
                    Api::TYPE_GET, '/api/locations/localities', [
                    'filter'   => $filter,
                    'per_page' => 250,
                    'page'     => 1,
                ], false
                )->getResult();

                $helper->log(json_encode($localityData, JSON_PRETTY_PRINT));
                $helper->log(
                    sprintf(
                        'Current offset value is "%s", count "%s", total records "%s"',
                        $resultOffset, $resultRecordCount, $totalRecords
                    )
                );

                if (empty($localityData['name'])) {
                    continue;
                }

                if (empty($result['page_count'])) {
                    $result = $api->setRequest($localityData)->request(
                        Api::TYPE_POST, '/api/locations/localities'
                    )->getResult();

                    $helper->log(
                        sprintf('Added locality %s', $result['id'])
                    );
                } else {
                    $locality = $result['_embedded']['localities'][0];

                    $patchUrl = parse_url(
                        $locality['_links']['self']['href'], PHP_URL_PATH
                    );
                    $helper->log(sprintf('Patch URL: %s', $patchUrl));

                    unset($localityData['extId']);
                    unset($localityData['country']);
                    unset($localityData['area']);

                    if (empty($locality['raw']['DGuid'])) {
                        $result = $api->setRequest($localityData)->request(
                            Api::TYPE_PATCH, $patchUrl
                        )->getResult();

                        $helper->log(
                            sprintf('Updated locality %s', $result['id'])
                        );
                    } else {
                        $helper->log(
                            sprintf('Locality %s exists', $result['id'])
                        );
                    }
                }
            }

            // Increment the offset for the next page
            $resultOffset += $resultRecordCount;
        } else {
            $helper->log(
                "Error fetching data for page starting at offset $resultOffset"
            );
            break; // Exit the loop on error
        }
    }
} else {
    echo "Error getting record count\n";
}

die();

$result = $api->request(
    Api::TYPE_GET, '/api/users/users/0'
)->getResult();

$helper->log(sprintf('Working user: %s', $result['id']));

$startPage = $totalPages = $argv[2] ?? 1;

$filter = [
    [
        'field' => 'postcode',
        'type'  => 'isnull',
    ],
];

if (!empty($argv[1])) {
    $filter[] = [
        'field' => 'country',
        'type'  => 'eq',
        'value' => $argv[1],
    ];
}

for ($page = $startPage; $page <= $totalPages; $page++) {
    $result = $api->request(
        Api::TYPE_GET, '/api/locations/localities', [
        'filter'   => $filter,
        'per_page' => 250,
        'page'     => $page,
    ], false
    )->getResult();

    if ($totalPages != $result['page_count']) {
        $totalPages = $result['page_count'];
    }

    $httpClient = new Client();
    $provider = new \Geocoder\Provider\GoogleMaps\GoogleMaps(
        $httpClient, null, $config['google-api-key']
    );
    $geocoder = new \Geocoder\StatefulGeocoder($provider, 'en');

    $i = 1;
    foreach ($result['_embedded']['localities'] as $locality) {
        $helper->log(
            sprintf(
                'Updating locality: %s (area %s)', $locality['name'],
                $locality['_embedded']['area']['name']
            )
        );

        $geo = $geocoder->geocodeQuery(
            GeocodeQuery::create(
                sprintf(
                    '%s %s, %s, %s %s', $locality['type'],
                    $locality['name'],
                    $locality['_embedded']['area']['name'],
                    $locality['postcode'],
                    $locality['_embedded']['country']['name']
                )
            )
        );

        if (count($geo->getIterator()) > 1) {
            $helper->log(sprintf('Several matches found'));

            continue;
        }

        /** @var Location $location */
        foreach ($geo->getIterator() as $location) {
            $helper->log(
                sprintf(
                    'Found match: %s (id %d)', $locality['name'],
                    $locality['id']
                )
            );

            $updateData = [];
            if (empty($locality['postcode'])
                && !empty(
                $location->getPostalCode()
                )
            ) {
                $updateData['postcode'] = $location->getPostalCode();
            }

            if (empty($locality['geo'])) {
                $updateData['geo'] = [
                    'latitude'  => $location->getCoordinates()
                        ->getLatitude(),
                    'longitude' => $location->getCoordinates()
                        ->getLongitude(),
                ];
            }

            if (!empty($updateData)) {
                $res = $api->setRequest($updateData)->request(
                    Api::TYPE_PATCH, sprintf(
                        '/api/locations/localities/%s', $locality['id']
                    )
                )->getResult();

                $helper->log(
                    sprintf(
                        'Locality %s was updated: %s', $res['id'],
                        var_export($updateData, true)
                    )
                );
            }
        }
    }
}
