<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$file = './examples/dev-data/orders.xlsx';
if (!empty($file)) {
    $reader = new Xlsx();
    $spreadsheet = $reader->load($file);

    $helper = new Sample();
    foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
        $dataKeys = [];
        foreach ($worksheet->getRowIterator() as $row) {
            $helper->log('    Row number - ' . $row->getRowIndex());

            if ($row->getRowIndex() < 5) {
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(
                false
            ); // Loop all cells, even if it is not set

            $i = 0;
            $data = [];
            foreach ($cellIterator as $cell) {
                if ($cell !== null) {
//                    $helper->log(
//                        '        Cell - ' . $cell->getCoordinate() . ' - '
//                        . $cell->getValue()
//                    );

                    $data[$cell->getColumn()] = $cell->getValue();
                }
            }

            $data = array_filter($data);

            if (empty($data['B']) && empty(array_filter($data))) {
                continue;
            }

            $orderData = [
                'state'  => 'download',
                'extId'  => trim($data['B']),
                'date'   => date('Y/m/d'),
                'shop'   => 203481,
                'source' => 9032,
            ];

//            if (!in_array($returnData['extId'], [
//                'X003D6Z775',
//                'X003RB9V91',
//            ])) {
//                continue;
//            }

            $result = $api->request(
                Api::TYPE_GET, '/api/products/order', [
                    'filter'   => [
                        [
                            'field' => 'source',
                            'type'  => 'eq',
                            'value' => $orderData['source'],
                        ],
                        [
                            'field' => 'extId',
                            'type'  => 'eq',
                            'value' => $orderData['extId'],
                        ],
                        //            [
                        //                'field'  => 'id',
                        //                'type'   => 'notIn',
                        //                'values' => $protected,
                        //            ],
                    ],
                    'fields'   => [
                        'id', 'state', 'shipmentDate',
                    ],
                    'per_page' => 250,
                    'order-by' => [
                        [
                            'type'      => 'field',
                            'field'     => 'shipmentDate',
                            'direction' => 'asc',
                        ],
                    ],
                ]
            )->getResult();

            if ($result['total_items'] == 0) {
                $result = $api->setRequest($orderData)->request(
                    Api::TYPE_POST, '/api/products/order'
                )->getResult();

                $helper->log(
                    sprintf(
                        'Created order: %s', $result['id']
                    )
                );
            } else {
                $helper->log(
                    sprintf(
                        'Order "%s" exists', $orderData['extId']
                    )
                );
            }
        }
    }
}
