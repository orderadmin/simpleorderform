<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.uploader.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$config = array_merge($config, [
    'mapping' => [
        'search' => [
            [
                'field'  => 'article',
                'column' => 'G',
            ],
        ],
        'update' => [
            [
                'field'  => 'barcodes',
                'column' => 'T',
            ],
        ],
    ],
]);

$file = './examples/dev-data/barcodes.xlsx';
if (!empty($file)) {
    $reader = new Xlsx();
    $spreadsheet = $reader->load($file);

    $helper = new Sample();
    foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
        $helper->log('Worksheet - ' . $worksheet->getTitle());

        $dataKeys = [];
        foreach ($worksheet->getRowIterator() as $row) {
            $helper->log('    Row number - ' . $row->getRowIndex());

            if ($row->getRowIndex() === 1) {
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(
                false
            ); // Loop all cells, even if it is not set

            $i = 0;
            $data = [];
            foreach ($cellIterator as $cell) {
                if ($cell !== null) {
//                    $helper->log(
//                        '        Cell - ' . $cell->getCoordinate() . ' - '
//                        . $cell->getValue()
//                    );

                    $data[$cell->getColumn()] = $cell->getValue();
                }
            }

            $filter = [];
            foreach ($config['mapping']['search'] as $mapping) {
                if (!empty($data[$mapping['column']])) {
                    $filter[] = [
                        'field' => $mapping['field'],
                        'type'  => 'eq',
                        'value' => $data[$mapping['column']],
                    ];
                }
            }

            if (!empty($filter)) {
                try {
                    $result = $api->request(
                        Api::TYPE_GET, '/api/products/offer', [
                        'filter' => $filter,
                    ], false
                    )->getResult();

                    if ($result['total_items'] == 0) {
                        $helper->log(var_export($filter, true));
                    } else {
                        if ($result['total_items'] == 1) {
                            //$helper->log(var_export($result, true));
                            $helper->log(var_export($filter, true));

                            foreach (
                                $result['_embedded']['product_offer'] as
                                $productOfferData
                            ) {
                                $update = [];
                                foreach (
                                    $config['mapping']['update'] as $mapping
                                ) {
                                    if (!empty($data[$mapping['column']])) {
                                        if (is_array(
                                                $productOfferData[$mapping['field']]
                                            )
                                            && in_array(
                                                $data[$mapping['column']],
                                                $productOfferData[$mapping['field']]
                                            )
                                        ) {
                                            continue;
                                        }

                                        if (is_array(
                                            $productOfferData[$mapping['field']]
                                        )
                                        ) {
                                            $update[$mapping['field']]
                                                = $productOfferData[$mapping['field']];
                                            $update[$mapping['field']][]
                                                = $data[$mapping['column']];
                                        } else {
                                            $update[$mapping['field']]
                                                = $data[$mapping['column']];
                                        }
                                    }
                                }

                                if (!empty($update)) {
                                    $update['shop']
                                        = $productOfferData['_embedded']['shop']['id'];

                                    $result = $api->setRequest($update)
                                        ->request(
                                            Api::TYPE_PATCH, sprintf(
                                                '/api/products/offer/%s/%s',
                                                $productOfferData['_embedded']['shop']['id'],
                                                $productOfferData['id']
                                            )
                                        )->getResult();

                                    $helper->log(
                                        sprintf(
                                            'Updated offer: %s', $result['id']
                                        )
                                    );
                                } else {
                                    $helper->log(
                                        sprintf(
                                            'Update is empty'
                                        )
                                    );
                                }
                            }
                        } else {
                            $helper->log('Duplicated offer found');
//                            $helper->log(var_export($result, true));
                            continue;
                        }
                    }
                } catch (\Exception $e) {
                    $helper->log('Offer not updated');
                    $helper->log($e->getMessage());
                    continue;
                }
            }
        }
    }
}
