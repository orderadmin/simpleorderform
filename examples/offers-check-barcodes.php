<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.uploader.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();
?>
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <style type="text/css">
        .table-borderless td,
        .table-borderless th {
            border: 0;
        }
    </style>

    <form enctype="multipart/form-data" class="form-inline m-3" method="POST">
        <div class="input-group mb-2 mr-sm-2">
            <input type="file" name="data" class="form-control"/>
        </div>
        <div class="input-group mb-2 mr-sm-2">
            <input type="text" name="limit" class="form-control"/>
        </div>
        <input type="submit" value="Upload" class="btn btn-primary mb-2"/>
    </form>

    <?php
$file = $_FILES['data'];
if (!empty($file['tmp_name'])) {
    $reader = new Xlsx();
    $spreadsheet = $reader->load($file['tmp_name']);

    $helper = new Sample();
    foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
        $helper->log('Worksheet - ' . $worksheet->getTitle());

        $dataKeys = [];
        foreach ($worksheet->getRowIterator() as $row) {
            $helper->log('    Row number - ' . $row->getRowIndex());

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(
                false
            ); // Loop all cells, even if it is not set

            $i = 0;
            $data = [];
            foreach ($cellIterator as $cell) {
                if ($cell !== null) {
                    $helper->log(
                        '        Cell - ' . $cell->getCoordinate() . ' - '
                        . $cell->getValue()
                    );

                    $data[$cell->getColumn()] = $cell->getValue();
                }

                $i++;

                if ($i = 10) {
                    break;
                }
            }

            if (!empty($data['A'])) {
                $offerData = [
                    'shop'     => $config['shop'],
                    'barcodes' => [
                        $data['A'],
                    ],
                ];

                try {
                    $result = $api->request(
                        Api::TYPE_GET, '/api/products/offer', [
                        'filter' => [
                            [
                                'field' => 'shop',
                                'type'  => 'eq',
                                'value' => $offerData['shop'],
                            ],
                            [
                                'field' => 'barcodes::text',
                                'type'  => 'like',
                                'value' => $data['A'],
                            ],
                        ],
                    ], false
                    )->getResult();

                    if ($result['total_items'] == 0) {
//                    $helper->log(var_export($result, true));
                        $helper->log(var_export($offerData, true));
                    }
                } catch (\Exception $e) {
                    echo $e->getMessage() . "\n";

                    continue;
                }
            }
        }
    }
}
