<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$result = $api->request(
    Api::TYPE_GET, '/api/accounts/invoice', [
        'filter'   => [
            [
                'field' => 'id',
                'type'  => 'gte',
                'value' => 1100,
            ],
            [
                'field' => 'state',
                'type'  => 'eq',
                'value' => 'paid',
            ],
        ],
        //        'fields'   => [
        //            'id', 'state', 'shipmentDate',
        //        ],
        'per_page' => 500,
        'order-by' => [
            [
                'type'      => 'field',
                'field'     => 'id',
                'direction' => 'asc',
            ],
        ],
    ]
)->getResult();

$helper->log(sprintf('Found %s invoices', $result['total_items']));

$invoices = $result['_embedded']['invoice'];
foreach ($invoices as $invoice) {
    $transactionResult = $api->request(
        Api::TYPE_GET, '/api/accounts/transaction', [
            'filter'   => [
                [
                    'field' => 'entityType',
                    'type'  => 'eq',
                    'value' => 'invoice',
                ],
                [
                    'field' => 'entity',
                    'type'  => 'eq',
                    'value' => $invoice['id'],
                ],
            ],
            //        'fields'   => [
            //            'id', 'state', 'shipmentDate',
            //        ],
            'per_page' => 50,
        ]
    )->getResult();

    if (empty($transactionResult['total_items'])) {
//        $helper->log(var_export($invoice, true));

        $result = $api->setRequest(
            [
                'extId'           => $invoice['id'],
                'state'           => 'confirmed',
                'rateModifier'    => 167,
                'value'           => $invoice['value'],
                'account'         => $invoice['_embedded']['account']['id'],
                'entityType'      => 'invoice',
                'entity'          => $invoice['id'],
                'transactionDate' => $invoice['created']['date'],
                'currency'        => 1,
                'invoice'         => $invoice['id'],
            ]
        )->request(
            Api::TYPE_POST,
            '/api/accounts/transaction'
        )->getResult();

        $helper->log(
            sprintf('Added transaction %s', $result['id'])
        );
    } else {
        $helper->log(
            sprintf(
                'An invoice %s have %d transactions',
                $invoice['id'],
                $transactionResult['total_items']
            )
        );
    }
}