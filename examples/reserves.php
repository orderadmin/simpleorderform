<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$result = $api->request(
    Api::TYPE_GET, '/api/storage/reserve', [
        'filter'     => [
            [
                'field' => 'state',
                'type'  => 'eq',
                'value' => 'distribution',
            ],
            //            [
            //                'field' => 'id',
            //                'type'  => 'lte',
            //                'value' => '627287',
            //            ],
            [
                'field' => 'state',
                'type'  => 'neq',
                'value' => 'cancel',
            ],
            //            [
            //                'field'  => 'id',
            //                'type'   => 'notIn',
            //                'values' => $protected,
            //            ],
        ],
        //        'fields'   => [
        //            'id', 'state', 'shipmentDate',
        //        ],
        'per_page'   => 500,
        'reserve-by' => [
            [
                'type'      => 'field',
                'field'     => 'created',
                'direction' => 'asc',
            ],
        ],
    ]
)->getResult();

$helper->log(sprintf('Found %s reserves', $result['total_items']));

$reserves = $result['_embedded']['reserve'];
foreach ($reserves as $reserve) {
//    $helper->log(var_export($reserve, true));

    if (!empty($reserve['_embedded']['order']['state'])
        && in_array($reserve['_embedded']['order']['state'], [
            'assembled', 'processing', 'delivery', 'complete', 'return',
        ])
    ) {
        $newState = 'complete';

        $result = $api->setRequest(
            [
                'state' => $newState
            ]
        )->request(
            Api::TYPE_PATCH,
            sprintf('/api/storage/reserve/%s', $reserve['id'])
        )->getResult();

        if ($result['state'] == $newState) {
            $helper->log(
                sprintf('Reserve %s: %s', $reserve['id'], $newState)
            );
        }
    } else {
        $helper->log(
            sprintf(
                'An order %s (reserve %s) is in "%s" state',
                $reserve['_embedded']['order']['id'],
                $reserve['id'],
                $reserve['_embedded']['order']['state']
            )
        );
    }
}