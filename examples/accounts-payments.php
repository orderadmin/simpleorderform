<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once('./examples/config.admin.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

unset($accessToken);

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id'  => $config['client'],
                'domain'     => $config['domain'],
                'grant_type' => 'password',
                'username'   => $config['user'],
                'password'   => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$helper = new Sample();

$result = $api->request(
    Api::TYPE_GET, '/api/accounts/payment', [
        'filter'   => [
            [
                'field' => 'owner',
                'type'  => 'eq',
                'value' => '11121',
            ],
        ],
        //        'fields'   => [
        //            'id', 'state', 'shipmentDate',
        //        ],
        'per_page' => 500,
        'order-by' => [
            [
                'type'      => 'field',
                'field'     => 'id',
                'direction' => 'asc',
            ],
        ],
    ]
)->getResult();

$helper->log(sprintf('Found %s payments', $result['total_items']));

$payments = $result['_embedded']['payment'];
foreach ($payments as $payment) {
    $report = $payment['_embedded']['report'];

    $helper->log(sprintf('Payment %s value is %s', $payment['id'], $payment['value']));

    $helper->log(sprintf('Report %s value is %s', $report['id'], $report['result']['total']['payment']));

    if ($payment['value'] != $report['result']['total']['payment']) {
        $difference = $report['result']['total']['payment'] - $payment['value'];

        $helper->log(sprintf('Difference is %s', $difference));

        if ($difference > 0) {
            $result = $api->setRequest(
                [
                    'value' => $report['result']['total']['payment'],
                ]
            )->request(
                Api::TYPE_PATCH,
                sprintf('/api/accounts/payment/%s', $payment['id'])
            )->getResult();

            $helper->log(
                sprintf('Updated transaction %s', $result['id'])
            );

            die();
        } else {

            echo 1;die();
        }
    }
}