<?php

namespace App\Lib;

class Api
{
    const TYPE_GET = 'GET';
    const TYPE_POST = 'POST';
    const TYPE_PATCH = 'PATCH';
    const TYPE_PUT = 'PUT';

    protected $user;
    protected $password;
    protected $token;
    protected $json;
    protected $result;
    protected $server = 'https://pro.oawms.com';
    protected $timeout = 30;
    protected $error;

    public function __construct($config)
    {
        $this->server = $config['server'];
    }

    public function setAccessToken(string $token)
    {
        $this->token = $token;

        return $this;
    }

    public function getResult($format = true)
    {
        if ($format) {
            return json_decode($this->result, true);
        } else {
            return $this->result;
        }
    }

    public function getError()
    {
        return $this->error;
    }

    public function setRequest($array)
    {
        $this->json = json_encode($array);

        return $this;
    }

    public function setJsonRequest($json)
    {
        $this->json = $json;

        return $this;
    }

    public function getRequest()
    {
        return $this->json;
    }

    public function request($type, $url, $query = [], $debug = false)
    {
        $ch = curl_init();

        if (strpos($url, $this->server) === false) {
            $url = $this->server . $url;
        }

        if (!empty($query)) {
            $url = $url . '?' . http_build_query($query);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
            'Content-Type: application/json',
            'Accept: application/json',
        ];

        if (!empty($this->token)) {
            $headers[] = 'Authorization: Bearer ' . $this->token;
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);

        if (!empty($this->user) && !empty($this->password)) {
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt(
                $ch, CURLOPT_USERPWD, $this->user . ":" . $this->password
            );
        }

        if (in_array($type, [self::TYPE_POST, self::TYPE_PATCH, self::TYPE_PUT]
        )
        ) {
            $data = $this->getRequest();

            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        if ($debug) {
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt(
                $ch, CURLOPT_STDERR, $verbose = fopen('php://temp', 'rw+')
            );
        }

        $this->result = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($debug) {
            echo '<pre>';
            print_r($query);
            echo '</pre>';

            echo http_build_query($query);

            var_dump(
                json_decode(
                    $this->getRequest(),
                    JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE
                )
            );

            echo '<hr />';
            echo $this->getRequest();
            echo '<hr />';

            echo '<pre>';
            echo $this->result;
            print_r(curl_getinfo($ch));
            echo '</pre>';

            die();
        }

        curl_close($ch);

        if (in_array($httpCode, array(200, 201))) {
            return $this;
        } else {
            $result = $this->getResult();

            $description = '';
            if (!empty($result['detail'])) {
                $description = $result['detail']
                    . (!empty($result['validation_messages']) ? var_export(
                        $result['validation_messages'], true
                    ) : '');
            }

            throw new \Exception($description, !empty($result['status']) ? $result['status'] : 500);
        }
    }
}
