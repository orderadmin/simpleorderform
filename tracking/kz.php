<?php

namespace App;

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

chdir(dirname(__DIR__));

require_once __DIR__ . '/../vendor/autoload.php';

//$config = require_once('config.uploader.php');
//
//if (!file_exists($config['cacheDir'])) {
//    mkdir($config['cacheDir'], 0777);
//}
//
//$api = new Api($config);
//
//$cache = new Filesystem();
//$cache->getOptions()->setTtl(3600);
//$cache->getOptions()->setCacheDir($config['cacheDir']);
//
//$plugin = new ExceptionHandler();
//$plugin->getOptions()->setThrowExceptions(false);
//$cache->addPlugin($plugin);
//
//$accessToken = $cache->getItem('access_token');
//
//if (empty($accessToken) && !empty($config['user'])
//    && !empty($config['password'])
//) {
//    try {
//        $oauth = $api->setRequest(
//            [
//                'client_id'  => $config['client'],
//                'domain'     => $config['domain'],
//                'grant_type' => 'password',
//                'username'   => $config['user'],
//                'password'   => $config['password'],
//            ]
//        )->request(Api::TYPE_POST, '/oauth', [])->getResult();
//
//        $cache->getOptions()->setTtl($oauth['expires_in']);
//
//        $cache->setItems($oauth);
//
//        $accessToken = $oauth['access_token'];
//    } catch (\Exception $e) {
//        echo '<strong>' . $e->getMessage() . '</strong>';
//
//        die();
//    }
//} elseif (empty($accessToken)) {
//    echo 'No access token';
//
//    die();
//}
//
//$api->setAccessToken($accessToken);

$reader = new Xlsx();
$spreadsheet = $reader->load(__DIR__ . '/1.xlsx');

$helper = new Sample();
foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
    $helper->log('Worksheet - ' . $worksheet->getTitle());

    foreach ($worksheet->getRowIterator() as $row) {
        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(
            false
        ); // Loop all cells, even if it is not set

        $rowData = [];
        foreach ($cellIterator as $cell) {
            $value = $cell->getCalculatedValue();
            if (strpos($value, 'LH') !== false) {
                $rowData[] = trim($value);
            }
        }

        if (!empty($rowData)) {
            echo join("\n", $rowData) . "\n";
        }
    }
}
