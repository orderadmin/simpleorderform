<?php
namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\Reader\Xls;

chdir(dirname(__DIR__));

require_once __DIR__ . '/vendor/autoload.php';

$config = require_once('config.uploader.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id' => $config['client'],
                'domain' => $config['domain'],
                'grant_type' => 'password',
                'username' => $config['user'],
                'password' => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';

        die();
    }
} elseif (empty($accessToken)) {
    echo 'No access token';

    die();
}

$api->setAccessToken($accessToken);

$reader = new Xls();
$spreadsheet = $reader->load(__DIR__ . '/superposuda.xls');

$helper = new Sample();
foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
    $helper->log('Worksheet - ' . $worksheet->getTitle());

    foreach ($worksheet->getRowIterator() as $row) {
        $helper->log('    Row number - ' . $row->getRowIndex());

        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set

        $row = [];
        foreach ($cellIterator as $cell) {
            if ($cell !== null) {
                $helper->log('        Cell - ' . $cell->getCoordinate() . ' - ' . $cell->getCalculatedValue());

                $row[] = $cell->getCalculatedValue();
            }
        }

        if (!empty($row[0])) {
            $cacheKey = sprintf('offer_%s', md5($row[0]));

            $offer = $cache->getItem($cacheKey);
            if (empty($offer)) {
                try {
                    $result = $api->request(
                        Api::TYPE_GET, '/api/products/offer', [
                        'filter' => [
                            [
                                'field' => 'article',
                                'type'  => 'eq',
                                'value' => $row[0],
                            ],
                        ],
                    ], false)->getResult();

                    if ($result['total_items'] == 1) {
                        $offer = $result['_embedded']['product_offer'][0];

                        $cache->setItem($cacheKey, json_encode($offer));

                        $barcodes = [];
                        if (empty($offer['barcodes'])) {
                            $offer['barcodes'] = [];
                        }

                        if (!in_array($row[2], $barcodes)) {
                            $barcodes[] = $row[2];

                            $update = $api->setRequest(
                                [
                                    'barcodes' => $barcodes,
                                ]
                            )->request(
                                Api::TYPE_PATCH,
                                sprintf('/api/products/offer/%s', $offer['id']),
                                [], false
                            )->getResult();
                        }
                    }
                } catch (\Exception $e) {
                    echo '<strong>' . $e->getMessage() . '</strong>';

                    die();
                }
            }
        }
    }
}