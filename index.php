<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 26.11.15
 * Time: 4:04
 */

error_reporting(0);

require_once('lib/orderadmin.php');

$config = require_once('config.default.php');

$api = new OrderadminAPI($config['server'], $config['user'], $config['secret']);

try {
    $data = $api->request(
        OrderadminAPI::TYPE_GET, '/api/products/offer', array(
            'fields' => [
                'id',
                'name',
                'type',
                'extId',
                'image',
                'sku',
                'price',
                'items',
            ],
            'filter' => [
                [
                    'type'       => 'orx',
                    'conditions' => [
                        ['field' => 'shop', 'type' => 'eq', 'value' => 34350],
                    ],
                    'where'      => 'and',
                ]
            ],
        )
    )->getResult();
} catch (\Exception $e) {
    echo '<b>' . $e->getMessage() . '</b>';
}

$products = $data['_embedded']['product_offer'];

try {
    $return = array();
    switch ($_REQUEST['action']) {
        case 'get-localities':
            $return = $api->request(
                OrderadminAPI::TYPE_GET, '/api/locations/localities', array(
                    'criteria' => $_REQUEST['criteria'],
                    'page'     => !empty($_REQUEST['page']) ? $_REQUEST['page']
                        : 1,
                    'sort'     => array('name' => 'asc'),
                )
            )->getResult();
            break;

        case 'get-service-points':
            $return = $api->request(
                OrderadminAPI::TYPE_GET,
                '/api/delivery-services/service-points', array(
                    'criteria' => $_REQUEST['criteria'],
                    'page'     => !empty($_REQUEST['page']) ? $_REQUEST['page']
                        : 1,
                    'sort'     => array('name' => 'asc'),
                )
            )->getResult();
            break;

        case 'order':
            $rawData = file_get_contents("php://input");
            $data = json_decode($rawData, true);
            $orderProducts = [];
            $orderPrice = 0;


//            var_dump($data);die;


            if (!empty($data['product'])) {
                foreach ($data['product'] as $value) {
                    foreach ($products as $key => $product) {
                        if ($product['id'] == $value) {
                            $orderProducts [] = [
                                'productOffer'  => [
                                    'extId'    => $product['extId'],
                                    'name'     => $product['name'],
                                    'type'     => $product['type'],
                                    'barcodes' => array_filter(
                                        $product['barcodes']
                                    ),
                                ],
                                'count'         => 1,
                                'price'         => !empty($product['price'])
                                    ? $product['price'] : 0,
                                'tax'           => null,
                                'discountPrice' => null,
                                'total'         => $product['price'],
                            ];

                            $orderPrice = +$product['price'];
                            unset($products[$key]);
                        }
                    }

                }
            }

            $data['shop'] = $config['shop'];
            $data['extId'] = time();
            $data['date'] = date('Y-m-d');
            $data['paymentState'] = 'paid';
            $data['orderPrice'] = 0;
            $data['totalPrice'] = $data['orderPrice'];
            $data['address']['country'] = 25;
            $data['recipientName'] = $data['profile']['name'];
            $data['recipientName'] = $data['profile']['email'];
            $data['orderProducts'] = $orderProducts;
            $data['eav']['delivery-services-request'] = true;
            $data['eav']['deliveryServiceData'] = [
                'deliveryService' => 'speedy'
            ];
            $data['profile']['extId'] = time();

            unset($data['product']);

//            var_dump($data['profile']);die;

            $return = $api->setRequest($data)->request(
                OrderadminAPI::TYPE_POST, '/api/products/order', array(), false
            )->getResult();
            break;

        case 'delivery-request':
            $rawData = file_get_contents("php://input");
            $data = json_decode($rawData, true);

            $data['sender'] = $config['sender'];
            $data['extId'] = mktime();
            $data['estimatedCost'] = 0;
            $data['payment'] = 0;

            if (!empty($data['recipient']['phone'])) {
                $data['recipientPhone'] = $data['recipient']['phone'];
            }

            $data['recipientAddress']['country'] = $config['country'];
            if (!empty($data['recipientAddress']['locality'])) {
                $data['recipientLocality']
                    = $data['recipientAddress']['locality'];
            }

            foreach ($data['places'] as &$place) {
                foreach ($place['items'] as &$item) {
                    $item['extId'] = $config['productOffer']['extId'];
                    $item['name'] = $config['productOffer']['name'];
                    $item['price'] = $config['productOffer']['price'];
                    $item['tax'] = !empty($config['productOffer']['tax'])
                        ? $config['productOffer']['tax'] : 0;
                    $item['total'] = $item['price'] * $item['count'];
                }

                $data['estimatedCost'] += $item['total'];
                $data['payment'] += $item['total'];
            }

            $return = $api->setRequest($data)->request(
                OrderadminAPI::TYPE_POST, '/api/delivery-services/requests',
                array(), false
            )->getResult();
            break;
    }

    if (!empty($return)) {
        header('Content-Type: application/json');
        echo json_encode($return);
        exit();
    }
} catch (Exception $e) {
    if (!function_exists('http_response_code')) {
        function http_response_code($newcode = null)
        {
            static $code = 200;
            if ($newcode !== null) {
                header('X-PHP-Response-Code: ' . $newcode, true, $newcode);
                if (!headers_sent()) {
                    $code = $newcode;
                }
            }
            return $code;
        }
    }

    header('Content-Type: application/json');
    http_response_code($e->getCode());

    echo json_encode(
        array(
            'type'   => 'http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html',
            'title'  => 'Error',
            'status' => $e->getCode(),
            'detail' => $e->getMessage(),
        )
    );

    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Title -->
    <title>Orderadmin</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- Google Fonts -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- CSS Global Compulsory -->
    <link rel="stylesheet"
          href="../../assets/vendor/bootstrap/bootstrap.min.css">
    <!-- CSS Global Icons -->
    <link rel="stylesheet"
          href="../../assets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="../../assets/vendor/icon-line/css/simple-line-icons.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
          rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css"
          rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.6.3/sweetalert2.min.css"
          rel="stylesheet"/>
    <link rel="stylesheet"
          href="../../assets/vendor/dzsparallaxer/dzsparallaxer.css">
    <link rel="stylesheet"
          href="../../assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
    <link rel="stylesheet"
          href="../../assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
    <link rel="stylesheet" href="../../assets/vendor/animate.css">
    <link rel="stylesheet"
          href="../../assets/vendor/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="../../assets/vendor/typedjs/typed.css">

    <!-- CSS Unify -->
    <link rel="stylesheet" href="../../assets/css/unify-core.css">
    <link rel="stylesheet" href="../../assets/css/unify-components.css">
    <link rel="stylesheet" href="../../assets/css/unify-globals.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="../../assets/css/custom.css">
</head>

<body>
<main>

    <section id="services" class="g-py-30--md">
        <div class="container text-center u-bg-overlay__inner g-mb-30 g-mb-70--md">
            <p style="text-align: center;">
                <img src="assets/img/logo.png" style="width: 550px;"/>
            </p>

            <div class="text-uppercase u-heading-v2-4--bottom g-brd-primary g-mb-30">
                <h2 class="u-heading-v2__title g-line-height-1 g-letter-spacing-2 g-font-weight-700 g-font-size-30 g-font-size-30--md g-theme-color-gray-dark-v1 mb-0">
                    Направете поръчка, сглобете я сами на нашият щанд и ще
                    получите поръчката си веднага тук</h2>
            </div>

            <p class="mb-0 g-font-size-20--md">OrderAdmin е софтуер, който
                автоматизира работата на склада и доставчиците. Този софтуер се
                нарича WMS (Warehouse Management System). Задачата на този сайт
                е да демонстрира работата на нашия софтуер в реално време, тук
                на изложението!
                Направете поръчката, сглобете я сами на нашият щанд и ще
                получите продуктите които си избрахте като подарък!
            </p>
        </div>
    </section>

    <section id="services" class="g-py-30--md">
        <div class="container text-center u-bg-overlay__inner g-max-width-770 g-mb-30 g-mb-70--md">
            <div class="text-uppercase u-heading-v2-4--bottom g-brd-primary g-mb-30">
                <h2 class="u-heading-v2__title g-line-height-1 g-letter-spacing-2 g-font-weight-700 g-font-size-30 g-font-size-40--md g-theme-color-gray-dark-v1 mb-0">
                    Как работи</h2>
            </div>
        </div>

        <div class="container">
            <!-- Row -->
            <div class="row">
                <div class="col-md-6 col-lg-3 g-mb-40 g-mb-0--lg">
                    <div class="text-center fadeInUp u-in-viewport"
                         data-animation="fadeInUp"
                         data-animation-duration="1500"
                         style="animation-duration: 1500ms;">
                        <span class="u-icon-v2 u-icon-size--3xl g-font-size-30 g-font-size-40--md g-color-primary g-brd-10 g-brd-gray-light-v5 g-rounded-50x g-mb-25"><i>1</i></span>
                        <h3 class="h6 text-uppercase g-font-weight-700 g-font-secondary g-color-black g-mb-15">
                            Избирате продукт и правите поръчка</h3>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3 g-mb-40 g-mb-0--lg">
                    <div class="text-center fadeInUp u-in-viewport"
                         data-animation="fadeInUp"
                         data-animation-duration="1750"
                         style="animation-duration: 1750ms;">
                        <span class="u-icon-v2 u-icon-size--3xl g-font-size-30 g-font-size-40--md g-color-primary g-brd-10 g-brd-gray-light-v5 g-rounded-50x g-mb-25"><i>2</i></span>
                        <h3 class="h6 text-uppercase g-font-weight-700 g-font-secondary g-color-black g-mb-15">
                            Сглобявате поръчката тук на нашият щанд</h3>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3 g-mb-40 g-mb-0--md">
                    <div class="text-center fadeInUp u-in-viewport"
                         data-animation="fadeInUp"
                         data-animation-duration="2000"
                         style="animation-duration: 2000ms;">
                        <span class="u-icon-v2 u-icon-size--3xl g-font-size-30 g-font-size-40--md g-color-primary g-brd-10 g-brd-gray-light-v5 g-rounded-50x g-mb-25"><i>3</i></span>
                        <h3 class="h6 text-uppercase g-font-weight-700 g-font-secondary g-color-black g-mb-15">
                            Опаковате поръчката тук на нашият щанд</h3>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="text-center fadeInUp u-in-viewport"
                         data-animation="fadeInUp"
                         data-animation-duration="2250"
                         style="animation-duration: 2250ms;">
                        <span class="u-icon-v2 u-icon-size--3xl g-font-size-30 g-font-size-40--md g-color-primary g-brd-10 g-brd-gray-light-v5 g-rounded-50x g-mb-25"><i>4</i></span>
                        <h3 class="h6 text-uppercase g-font-weight-700 g-font-secondary g-color-black g-mb-15">
                            Получавате поръчката веднага(или по куриер)</h3>
                    </div>
                </div>
            </div>
            <!-- End Row -->
        </div>
    </section>

    <!-- products -->
    <div class="container">
        <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>?action=order"
              method="post" data-target="orderadmin">
            <div class="container">

                <div class="form-group g-mb-20">
                    <label class="g-mb-10" for="name">Име и фамилия</label>
                    <input id="name" name="profile[name]"
                           class="form-control form-control-md rounded-0"
                           type="text" placeholder="Име и фамилия" required>
                </div>

                <div class="form-group g-mb-25">
                    <label for="phone">Телефон за връзка</label>
                    <input type="tel" name="profile[phones]"
                           class="form-control rounded-0 form-control-md"
                           id="phone" placeholder="Телефон за връзка"
                           value="0876988321" required>
                </div>

                <input type="hidden" name="profile[phones]"
                       value="0876988321" required>

                <div class="form-group g-mb-25">
                    <label for="city">Град</label>
                    <select class="form-control rounded-0"
                            name="address[locality]" id="city" value="181286">
                    </select>
                </div>

                <input type="hidden" name="address[locality]"
                       id="city" value="181286" required>

                <div class="form-group g-mb-25">
                    <label for="addr">Адрес</label>
                    <input type="text" name="address[street]"
                           class="form-control rounded-0 form-control-md"
                           id="addr" placeholder="Адрес" value="eComm Congress"
                           required>
                </div>

                <input type="hidden" name="address[street]"
                       id="addr" placeholder="Адрес" value="eComm Congress"
                       required>

                <div class="form-group g-mb-25">
                    <label for="pk">Пощенски код</label>
                    <input type="text" name="address[postcode]"
                           class="form-control rounded-0 form-control-md"
                           id="pk" placeholder="Пощенски код" value="1000"
                           required>
                </div>

                <input type="hidden" name="address[postcode]"
                       id="pk" placeholder="Пощенски код" value="1000" required>

                <div class="form-group g-mb-25">
                    <label for="email">Email</label>
                    <input type="email"
                           class="form-control rounded-0 form-control-md"
                           id="email" aria-describedby="emailHelp"
                           placeholder="Email" name="profile[email]" required>
                </div>

                <input type="hidden"
                       id="email" aria-describedby="emailHelp"
                       placeholder="Email" name="profile[email]"
                       value="m@lemon.bg" required>

            </div>

            <div class="row">
                <?
                foreach ($products as $product) :
                    ?>

                    <div class="col-md-6 col-lg-3 g-mb-30">
                        <!-- Article -->
                        <article
                                class="u-shadow-v19 g-bg-white text-center rounded g-px-20 g-py-40 g-mb-5">
                            <!-- Article Image -->
                            <div>
                                <img class="d-inline-block img-fluid mb-4"
                                     src="<?= $product['image'] ?>"
                                     alt="<?= $product['name'] ?>"
                                     style="max-height: 190px;">
                                <!-- End Article Image -->
                            </div>
                            <!-- Article Content -->
                            <h4 class="h5 g-color-black g-font-weight-600 g-mb-10"><?= $product['name'] ?></h4>
                            <p>Артикул: <?= $product['sku'] ?></p>
                            <span class="d-block g-color-primary g-font-size-16">Наличност: <?= $product['items'] ?></span>
                            <span class="d-block g-font-size-16">Цена: <?= $product['price'] ?></span>


                            <div class="form-check">
                                <label class="form-check-label g-mb-20">
                                    <input type="checkbox"
                                           class="form-check-input mr-1"
                                           name="product[]"
                                           value="<?= $product['id'] ?>">
                                </label>
                            </div>


                            <!-- End Article Content -->
                        </article>
                        <!-- End Article -->
                    </div>

                <? endforeach; ?>

            </div>
            <input type="submit" value="Купи"
                   class="btn btn-md u-btn-primary g-mr-10 g-mb-15">
        </form>
    </div>
    <!-- products -->

    <!-- Copyright Footer -->
    <footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20">
        <div class="container">
            <div class="row">
                <div class="col-md-8 text-center text-md-left g-mb-10 g-mb-0--md">
                    <div class="d-lg-flex">
                        <small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md">
                            2019 © All Rights Reserved.
                        </small>
                    </div>
                </div>

                <div class="col-md-4 align-self-center">
                    <ul class="list-inline text-center text-md-right mb-0">
                        <li class="list-inline-item g-mx-10"
                            data-toggle="tooltip" data-placement="top"
                            title="Facebook">
                            <a href="https://web.facebook.com/orderadmin?_rdc=1&_rdr"
                               class="g-color-white-opacity-0_5 g-color-white--hover">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Copyright Footer -->
    <a class="js-go-to u-go-to-v1" href="#!" data-type="fixed" data-position='{
     "bottom": 15,
     "right": 15
   }' data-offset-top="400" data-compensation="#js-header"
       data-show-effect="zoomIn">
        <i class="hs-icon hs-icon-arrow-top"></i>
    </a>
</main>

<div class="u-outer-spaces-helper"></div>

<script src="../../assets/vendor/jquery/jquery.min.js"></script>
<script src="../../assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
<script src="../../assets/vendor/popper.min.js"></script>
<script src="../../assets/vendor/bootstrap/bootstrap.min.js"></script>
<script src="../../assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="../../assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="../../assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
<script src="../../assets/vendor/slick-carousel/slick/slick.js"></script>
<script src="../../assets/vendor/typedjs/typed.min.js"></script>
<script src="../../assets/js/hs.core.js"></script>
<script src="../../assets/js/components/hs.header.js"></script>
<script src="../../assets/js/components/hs.tabs.js"></script>
<script src="../../assets/js/components/hs.popup.js"></script>
<script src="../../assets/js/components/hs.carousel.js"></script>
<script src="../../assets/js/components/hs.go-to.js"></script>
<script src="../../assets/js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="<?php echo $config['server']; ?>/frontend/node_modules/form-serializer/dist/jquery.serialize-object.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.6.3/sweetalert2.all.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('input[name="eav[delivery-services-delivery-request-rate]"]').change(function (e) {
            e.preventDefault();

            var type = $('input[name="eav[delivery-services-delivery-request-rate]"]:checked').data('target');

            $('[data-target="eav[delivery-services-delivery-request-rate]"]').hide();
            $('[data-target="eav[delivery-services-delivery-request-rate]"] .form-control').removeAttr('required');
            $('#' + type + '-group').show();
            $('#' + type + '-group .form-control form-control-sm').prop('required', 'required');
        });

        $('input[name="rate"]').change(function (e) {
            e.preventDefault();

            var type = $('input[name="rate"]:checked').data('target');

            $('[data-target="rate"]').hide();
            $('[data-target="rate"] .form-control').removeAttr('required');
            $('#' + type + '-group').show();
            $('#' + type + '-group .form-control form-control-sm').prop('required', 'required');
        });

        $('form[data-target="orderadmin"]').submit(function (e) {
            e.preventDefault();

            var $form = $(this);
            var data = $(this).serializeJSON();

            swal({
                title: 'Изпращане на поръчка',
                onOpen: () => {
                    swal.showLoading()
                }
            });

            $form.find('[type=submit]').prop('disabled', 'disabled');

            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: data,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
            }).done(function (data) {
                swal('Поръчка бе направена', 'Изчакайте изпращане на стока, номер на поръчка - ' + data.id + '!', 'success');

                $form[0].reset();
            }).fail(function (xhr) {
                if (xhr.responseJSON != undefined) {
                    swal(xhr.status.toString(), xhr.responseJSON.detail, "error");
                } else {
                    swal(xhr.status.toString(), xhr.statusText, "error");
                }
            });
        });
    });
</script>

<!-- JS Plugins Init. -->
<script>
    $(document).on('ready', function () {
        // initialization of carousel
        $.HSCore.components.HSCarousel.init('.js-carousel');

        // initialization of tabs
        $.HSCore.components.HSTabs.init('[role="tablist"]');

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');


        $(function () {
            $('#citySelect').select2({
                ajax: {
                    url: '<?php echo $_SERVER['SCRIPT_NAME']; ?>?action=get-localities',
                    data: function (params) {
                        var query = {
                            criteria: {
                                country: <?php echo $config['country']; ?>,
                                name: params.term != undefined ? '%' + params.term + '%' : '%',
                            },
                            page: params.page != undefined ? params.page : 1,
                        };

                        return query;
                    },
                    processResults: function (data) {
                        var results = [];
                        results.results = [];

                        $.each(data._embedded.localities, function (i, locality) {
                            var text = [(locality.postcode ? '[' + locality.postcode + '] ' : '') + locality.name, locality._embedded.area != undefined ? locality._embedded.area.name : null, locality._embedded.country.name];

                            results.results.push({
                                id: locality.id,
                                text: text.filter(function (value) {
                                    return (value.length > 0);
                                }).join(', '),
                            });
                        });

                        if (data.page_count > 1) {
                            results.pagination = {
                                more: true,
                            };
                        }

                        return results;
                    }
                }
            });
        });
    });

    $(window).on('load', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));

    });

    $(window).on('resize', function () {
        setTimeout(function () {
            $.HSCore.components.HSTabs.init('[role="tablist"]');
        }, 200);
    });
</script>

</body>

</html>
