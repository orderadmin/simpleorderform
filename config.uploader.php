<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 28.02.2018
 * Time: 17:25
 */

return array(
    'server'       => 'https://panel.orderadmin.eu',
    'client'       => 'orderadmin',
    'domain'       => 'rw',
    'user'         => getenv('APP_USER') ?? 'user',
    'password'     => getenv('APP_PASSWORD') ?? 'password',
    'cacheDir'     => '.cache',
    'country'      => 25,
    'shop'         => 34350,
    'sender'       => 146,
    'productOffer' => array(
        'extId' => '1000pages',
        'name'  => '1000 страници България',
        'price' => 26.9,
    ),
);
