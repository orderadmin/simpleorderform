<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 26.11.15
 * Time: 4:04
 */

require_once('lib/orderadmin.php');

$config = require_once('config.default.php');

$api = new OrderadminAPI($config['server'], $config['user'], $config['secret']);

$data = $api->request(OrderadminAPI::TYPE_GET, '/api/products/offer', array(
    'filter' => [
        [
            'type' => 'orx',
            'conditions' => [
                ['field' => 'shop', 'type' => 'eq', 'value' => 30],
            ],
            'where' => 'and',
        ]
    ],
))->getResult();

$products = $data['_embedded']['product_offer'];

try {
    $return = array();
    switch ($_REQUEST['action']) {
        case 'get-localities':
            $return = $api->request(OrderadminAPI::TYPE_GET, '/api/locations/localities', array(
                'criteria' => $_REQUEST['criteria'],
                'page' => !empty($_REQUEST['page']) ? $_REQUEST['page'] : 1,
                'sort' => array('name' => 'asc'),
            ))->getResult();
            break;

        case 'get-service-points':
            $return = $api->request(OrderadminAPI::TYPE_GET, '/api/delivery-services/service-points', array(
                'criteria' => $_REQUEST['criteria'],
                'page' => !empty($_REQUEST['page']) ? $_REQUEST['page'] : 1,
                'sort' => array('name' => 'asc'),
            ))->getResult();
            break;

        case 'order':

            $rawData = file_get_contents("php://input");
            $data = json_decode($rawData, true);
            $orderProducts = [];
            $orderPrice = 0;

            if (!empty($data['product'])) {
                foreach ($data['product'] as $value) {

                    foreach ($products as $key => $product) {
                        if ($product['id'] == $value) {
                            $orderProducts [] = [
                                'productOffer'  => [
                                    'extId'    => $product['extId'],
                                    'name'     => $product['name'],
                                    'type'     => $product['type'],
                                    'barcodes' => array_filter($product['barcodes']),
                                ],
                                'count'         => 1,
                                'price'         => ! empty($product['price'])
                                    ? $product['price'] : 0,
                                'tax'           => null,
                                'discountPrice' => null,
                                'total'         => $product['price'],
                            ];

                            $orderPrice += $product['price'];
                            unset($products[$key]);
                        }
                    }
                }
            }




//            $data['order'] = [
//                'shop'              => $config['shop'],
//                'extId'             => time(),
//                'date'              => date('Y-m-d'),
//                'clientId'          => null,
//                'recipientName'     => join(
//                    ' ',
//                    array_filter(
//                        [! empty($customer['firstname'])
//                             ? $customer['firstname']
//                             : null,
//                         ! empty($customer['lastname'])
//                             ? $customer['lastname']
//                             : null]
//                    )
//                ),
//                //                        'status'            => ! empty($orderState['name'])
//                //                            ? $orderState['name'] : null,
//                'stateDescription'  => null,
//                'orderPrice'        => $orderPrice,
//                'discountPrice'     => null,
//                'totalPrice'        => $orderPrice,
//                'comment'           => null,
//                'shipmentDate'      => null,
//                'shippedByDocument' => null,
//                'raw'               => null,
//            ];






            $data['shop'] = $config['shop'];
            $data['extId'] = time();
            $data['date'] = date('Y-m-d');
            $data['paymentState'] = 'not_paid';
            $data['orderPrice'] = 0;
            $data['totalPrice'] = $data['orderPrice'];
            $data['address']['country'] = $config['locality'];

            $data['orderProducts'] = $orderProducts;

            unset($data['product']);
            unset($data['eav']);

//            var_dump($data);die;

            $return = $api->setRequest($data)->request(OrderadminAPI::TYPE_POST, '/api/products/order', array(), false)->getResult();
            break;

        case 'delivery-request':
            $rawData = file_get_contents("php://input");
            $data = json_decode($rawData, true);

            $data['sender'] = $config['sender'];
            $data['extId'] = mktime();
            $data['estimatedCost'] = 0;
            $data['payment'] = 0;

            if (!empty($data['recipient']['phone'])) {
                $data['recipientPhone'] = $data['recipient']['phone'];
            }

            $data['recipientAddress']['country'] = $config['country'];
            if (!empty($data['recipientAddress']['locality'])) {
                $data['recipientLocality'] = $data['recipientAddress']['locality'];
            }

            foreach ($data['places'] as &$place) {
                foreach ($place['items'] as &$item) {
                    $item['extId'] = $config['productOffer']['extId'];
                    $item['name'] = $config['productOffer']['name'];
                    $item['price'] = $config['productOffer']['price'];
                    $item['tax'] = !empty($config['productOffer']['tax']) ? $config['productOffer']['tax'] : 0;
                    $item['total'] = $item['price'] * $item['count'];
                }

                $data['estimatedCost'] += $item['total'];
                $data['payment'] += $item['total'];
            }

            $return = $api->setRequest($data)->request(OrderadminAPI::TYPE_POST, '/api/delivery-services/requests', array(), false)->getResult();
            break;
    }

    if (!empty($return)) {
        header('Content-Type: application/json');
        echo json_encode($return);
        exit();
    }
} catch (Exception $e) {
    if (!function_exists('http_response_code')) {
        function http_response_code($newcode = NULL)
        {
            static $code = 200;
            if ($newcode !== NULL) {
                header('X-PHP-Response-Code: ' . $newcode, true, $newcode);
                if (!headers_sent())
                    $code = $newcode;
            }
            return $code;
        }
    }

    header('Content-Type: application/json');
    http_response_code($e->getCode());

    echo json_encode(array(
        'type' => 'http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html',
        'title' => 'Error',
        'status' => $e->getCode(),
        'detail' => $e->getMessage(),
    ));

    exit();
}

?>
<html>
<head>
    <title>Поръчка</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css"
          rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.6.3/sweetalert2.min.css" rel="stylesheet"/>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
            integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
            integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
            crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="<?php echo $config['server']; ?>/frontend/node_modules/form-serializer/dist/jquery.serialize-object.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.6.3/sweetalert2.all.min.js"></script>

    <style type="text/css">
        /* Space out content a bit */
        body {
            padding-top: 1.5rem;
            padding-bottom: 1.5rem;
        }

        .check {
            background-color: #fff;
        }
        /* Everything but the jumbotron gets side spacing for mobile first views */
        .header,
        .forms,
        .footer {
            padding-right: 1rem;
            padding-left: 1rem;
        }

        /* Custom page header */
        .header {
            padding-bottom: 1rem;
            border-bottom: .05rem solid #e5e5e5;
        }

        /* Make the masthead heading the same height as the navigation */
        .header h3 {
            margin-top: 0;
            margin-bottom: 0;
            line-height: 3rem;
        }

        /* Custom page footer */
        .footer {
            padding-top: 1.5rem;
            color: #777;
            border-top: .05rem solid #e5e5e5;
        }

        /* Customize container */
        @media (min-width: 48em) {
            .container {
                max-width: 46rem;
            }
        }

        .container-narrow > hr {
            margin: 2rem 0;
        }

        /* Main forms message and sign up button */
        .jumbotron {
            text-align: center;
            border-bottom: .05rem solid #e5e5e5;
        }

        .jumbotron .btn {
            padding: .75rem 1.5rem;
            font-size: 1.5rem;
        }

        /* Supporting forms content */
        .forms {
            margin: 1rem 0;
        }

        .forms p + h4 {
            margin-top: 1.5rem;
        }

        .select2, .select2-container {
            min-width: 520px!important;
        }

        /* Responsive: Portrait tablets and up */
        @media screen and (min-width: 48em) {
            /* Remove the padding we set earlier */
            .header,
            .forms,
            .footer {
                padding-right: 0;
                padding-left: 0;
            }

            /* Space out the masthead */
            .header {
                margin-bottom: 2rem;
            }

            /* Remove the bottom border on the jumbotron for visual effect */
            .jumbotron {
                border-bottom: 0;
            }
        }
    </style>
</head>
<body>

<div class="container">
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills float-right" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#delivery-request" role="tab" data-toggle="tab">Delivery
                        request</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#order" role="tab" data-toggle="tab">Order</a>
                </li>
            </ul>
        </nav>
        <h3 class="text-muted">Форма за поръчка</h3>
    </div>

    <!--
    <div class="jumbotron">
        <h1 class="display-3">Jumbotron heading</h1>
        <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
        <p><a class="btn btn-lg btn-success" href="#" role="button">Sign up today</a></p>
    </div>
    -->

    <div class="row">
        <div class="col-lg-12">

            <!-- Tab panes -->
            <div class="forms tab-content">
                <div role="tabpanel" class="tab-pane active" id="delivery-request">
                    <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>?action=delivery-request" method="POST"
                          data-target="orderadmin">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="count">Брой книги</label>
                            <div class="col-sm-9">
                                <select class="form-control form-control-sm" name="places[0][items][0][count]"
                                        id="count" required>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
                                    <option>9</option>
                                    <option>10</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="name">Име и фамилия</label>
                            <div class="col-sm-9">
                                <input type="text" name="recipient[name]" class="form-control form-control-sm"
                                       placeholder="Име и фамилия" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="locality">Град</label>
                            <div class="col-sm-9">
                                <select class="form-control form-control-sm" name="recipientAddress[locality]"
                                        id="delivery-request-locality" required></select>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#delivery-request-locality').select2({
                                            ajax: {
                                                url: '<?php echo $_SERVER['SCRIPT_NAME']; ?>?action=get-localities',
                                                data: function (params) {
                                                    var query = {
                                                        criteria: {
                                                            country: <?php echo $config['country']; ?>,
                                                            name: params.term != undefined ? '%' + params.term + '%' : '%',
                                                        },
                                                        page: params.page != undefined ? params.page : 1,
                                                    };

                                                    return query;
                                                },
                                                processResults: function (data) {
                                                    var results = [];
                                                    results.results = [];

                                                    $.each(data._embedded.localities, function (i, locality) {
                                                        var text = [(locality.postcode ? '[' + locality.postcode + '] ' : '') + locality.name, locality._embedded.area != undefined ? locality._embedded.area.name : null, locality._embedded.country.name];

                                                        results.results.push({
                                                            id: locality.id,
                                                            text: text.filter(function (value) {
                                                                return (value.length > 0);
                                                            }).join(', '),
                                                        });
                                                    });

                                                    if (data.page_count > 1) {
                                                        results.pagination = {
                                                            more: true,
                                                        };
                                                    }

                                                    return results;
                                                }
                                            }
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="postcode">Пощенски код</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm"
                                       name="recipientAddress[postcode]"
                                       id="postcode"
                                       placeholder="Пощенски код">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="rate">Тип на доставка</label>
                            <div class="col-sm-9">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio"
                                           name="rate"
                                           data-target="service_point"
                                           value="81" required/>
                                    <label class="form-check-label" for="servicePoint">
                                        Доставка до офис
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio"
                                           name="rate"
                                           data-target="courier"
                                           value="86" required/>
                                    <label class="form-check-label" for="courier">
                                        Доставка до адрес
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="service_point-group"
                             data-target="rate"
                             style="display: none;">
                            <label class="col-sm-3 col-form-label" for="locality">Офис на куриер</label>
                            <div class="col-sm-9">
                                <select class="form-control form-control-sm" name="servicePoint"
                                        id="deliveryRequestServicePoint"
                                        style="width: 100%;"></select>
                                <small id="localityHelp" class="form-text text-muted">Choose locality from previous
                                    field to
                                    activate filter.
                                </small>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#deliveryRequestServicePoint').select2({
                                            ajax: {
                                                url: '<?php echo $_SERVER['SCRIPT_NAME']; ?>?action=get-service-points',
                                                data: function (params) {
                                                    var query = {
                                                        criteria: {
                                                            locality: $('#delivery-request-locality').val(),
                                                            deliveryService: 23,
                                                        },
                                                        page: params.page != undefined ? params.page : 1,
                                                    };

                                                    return query;
                                                },
                                                processResults: function (data) {
                                                    var results = [];
                                                    results.results = [];

                                                    $.each(data._embedded.servicePoints, function (i, servicePoint) {
                                                        var text = '[' + servicePoint.extId + '] ' + servicePoint.name + ' (' + servicePoint.rawAddress + ')';

                                                        results.results.push({
                                                            id: servicePoint.id,
                                                            text: text,
                                                        });
                                                    });

                                                    if (data.page_count > 1) {
                                                        results.pagination = {
                                                            more: true,
                                                        };
                                                    }

                                                    // Tranforms the top-level key of the response object from 'items' to 'results'
                                                    return results;
                                                }
                                            }
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="form-group row" id="courier-group"
                             data-target="rate"
                             style="display: none;">
                            <label class="col-sm-3 col-form-label" for="address">Точен адрес</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm"
                                       name="recipientAddress[notFormal]"
                                       id="address"
                                       placeholder="Точен адрес">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="phone">Телефон за връзка</label>
                            <div class="col-sm-9">
                                <input type="phone" class="form-control form-control-sm" name="recipient[phone]"
                                       id="phone"
                                       placeholder="Телефон за връзка" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="email">Email</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control form-control-sm" name="recipient[email]"
                                       id="email"
                                       placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-9 offset-sm-3">
                                <button type="submit" class="btn btn-primary btn-sm">Изпрати</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="order">
                    <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>?action=order" method="POST"
                          data-target="orderadmin">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="name">Име и фамилия</label>
                            <div class="col-sm-9">
                                <input type="text" name="profile[name]" class="form-control form-control-sm"
                                       placeholder="Име и фамилия">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="locality">Град</label>
                            <div class="col-sm-9">
                                <select class="form-control form-control-sm" name="address[locality]" id="locality"
                                        required></select>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#locality').select2({
                                            ajax: {
                                                url: '<?php echo $_SERVER['SCRIPT_NAME']; ?>?action=get-localities',
                                                data: function (params) {
                                                    var query = {
                                                        criteria: {
                                                            country: <?php echo $config['country']; ?>,
                                                            name: params.term != undefined ? '%' + params.term + '%' : '%',
                                                        },
                                                        page: params.page != undefined ? params.page : 1,
                                                    };

                                                    return query;
                                                },
                                                processResults: function (data) {
                                                    var results = [];
                                                    results.results = [];

                                                    $.each(data._embedded.localities, function (i, locality) {
                                                        var text = [(locality.postcode ? '[' + locality.postcode + '] ' : '') + locality.name, locality._embedded.area != undefined ? locality._embedded.area.name : null, locality._embedded.country.name];

                                                        results.results.push({
                                                            id: locality.id,
                                                            text: text.filter(function (value) {
                                                                return (value.length > 0);
                                                            }).join(', '),
                                                        });
                                                    });

                                                    if (data.page_count > 1) {
                                                        results.pagination = {
                                                            more: true,
                                                        };
                                                    }

                                                    return results;
                                                }
                                            }
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="postcode">Пощенски код</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm" name="address[postcode]"
                                       id="postcode"
                                       placeholder="Пощенски код">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="postcode">Тип на доставка</label>
                            <div class="col-sm-9">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio"
                                           name="eav[delivery-services-delivery-request-rate]"
                                           data-target="service_point"
                                           value="81" required/>
                                    <label class="form-check-label" for="servicePoint">
                                        Доставка до офис
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio"
                                           name="eav[delivery-services-delivery-request-rate]"
                                           data-target="courier"
                                           value="86"/>
                                    <label class="form-check-label" for="courier">
                                        Доставка до адрес
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="service_point-group"
                             data-target="eav[delivery-services-delivery-request-rate]"
                             style="display: none;">
                            <label class="col-sm-3 col-form-label" for="locality">Офис на куриер</label>
                            <div class="col-sm-9">
                                <select class="form-control form-control-sm"
                                        name="eav[delivery-services-delivery-request-service-point]"
                                        id="servicePoint"
                                        style="width: 100%;"></select>
                                <small id="localityHelp" class="form-text text-muted">Choose locality from previous
                                    field to
                                    activate filter.
                                </small>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#servicePoint').select2({
                                            ajax: {
                                                url: '<?php echo $_SERVER['SCRIPT_NAME']; ?>?action=get-service-points',
                                                data: function (params) {
                                                    var query = {
                                                        criteria: {
                                                            locality: $('#locality').val(),
                                                            deliveryService: 23,
                                                        },
                                                        page: params.page != undefined ? params.page : 1,
                                                    };

                                                    return query;
                                                },
                                                processResults: function (data) {
                                                    var results = [];
                                                    results.results = [];

                                                    $.each(data._embedded.servicePoints, function (i, servicePoint) {
                                                        var text = '[' + servicePoint.extId + '] ' + servicePoint.name + ' (' + servicePoint.rawAddress + ')';

                                                        results.results.push({
                                                            id: servicePoint.id,
                                                            text: text,
                                                        });
                                                    });

                                                    if (data.page_count > 1) {
                                                        results.pagination = {
                                                            more: true,
                                                        };
                                                    }

                                                    // Tranforms the top-level key of the response object from 'items' to 'results'
                                                    return results;
                                                }
                                            }
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="form-group row" id="courier-group"
                             data-target="eav[delivery-services-delivery-request-rate]"
                             style="display: none;">
                            <label class="col-sm-3 col-form-label" for="address">Точен адрес</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm" name="address[notFormal]"
                                       id="address"
                                       placeholder="Точен адрес">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="phone">Телефон за връзка</label>
                            <div class="col-sm-9">
                                <input type="phone" class="form-control form-control-sm" name="profile[phone]"
                                       id="phone"
                                       placeholder="Телефон за връзка">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="email">Email</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control form-control-sm" name="profile[email]"
                                       id="email"
                                       placeholder="Email">
                            </div>
                        </div>

                        <table width="100%">
                            <tbody>
                            <?
                            $i = 0;
                            foreach ($products as $product) :
                            $i++;
                            if ($i % 2 ) {

                            ?>
                            <tr>
                                <td class="td_goods" id="td_005"
                                    style="background-color: rgb(238, 238, 238);">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td class="check">
                                                <label class="gtr">
                                                    <input
                                                            id="ch_005" class="rtg"
                                                            type="checkbox"
                                                            name="product[]"
                                                            value="<?=$product['id']?>"
                                                            >
                                                    <span class="mrt"></span>
                                                </label>
                                            </td>
                                            <td>
                                                <img src="<?=$product['image']?>"
                                                     width="150"
                                                     style="border: 1px solid #DDDDDD;">
                                            </td>
                                            <td style="vertical-align: top; padding-left: 10px;">
                                                <b style="font-family:RobotoBold;font-size:23px;"><?=$product['name']?></b><br>
                                                Артикул: <?=$product['sku']?><br><br>
                                                <div id="msg_005"
                                                     style="color: red;"></div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <?
                                } else {
                                ?>
                                <td class="td_goods" id="td_002"
                                    style="background-color: rgb(238, 238, 238);">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td  class="check">
                                                <label class="gtr">
                                                    <input id="ch_002"
                                                                  class="rtg"
                                                                  type="checkbox"
                                                                  name="product[]"
                                                                  value="<?=$product['id']?>">
                                                    <span class="mrt"></span>
                                                </label>
                                            </td>
                                            <td>
                                                <img src="<?=$product['image']?>"
                                                     width="150"
                                                     style="border: 1px solid #DDDDDD;">
                                            </td>
                                            <td style="vertical-align: top; padding-left: 10px;">
                                                <b style="font-family:RobotoBold;font-size:23px;"><?=$product['name']?></b><br>
                                                Артикул: <?=$product['sku']?><br><br>
                                                <div id="msg_002"
                                                     style="color: red;"></div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <?
                                }
                                endforeach;
                                ?>
                            </tbody>
                        </table>


                        <div class="form-group row" style="margin-top: 20px;">
                            <div class="col-sm-9 offset-sm-5">
                                <button type="submit" class="btn btn-primary btn-sm">Изпрати</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <footer class="footer">
        <p>© <a href="https://orderadmin.eu">Orderadmin</a></p>
    </footer>
</div>

<script type="text/javascript">
    $(function () {
        $('input[name="eav[delivery-services-delivery-request-rate]"]').change(function (e) {
            e.preventDefault();

            var type = $('input[name="eav[delivery-services-delivery-request-rate]"]:checked').data('target');

            $('[data-target="eav[delivery-services-delivery-request-rate]"]').hide();
            $('[data-target="eav[delivery-services-delivery-request-rate]"] .form-control').removeAttr('required');
            $('#' + type + '-group').show();
            $('#' + type + '-group .form-control form-control-sm').prop('required', 'required');
        });

        $('input[name="rate"]').change(function (e) {
            e.preventDefault();

            var type = $('input[name="rate"]:checked').data('target');

            $('[data-target="rate"]').hide();
            $('[data-target="rate"] .form-control').removeAttr('required');
            $('#' + type + '-group').show();
            $('#' + type + '-group .form-control form-control-sm').prop('required', 'required');
        });

        $('form[data-target="orderadmin"]').submit(function (e) {
            e.preventDefault();

            var $form = $(this);
            var data = $(this).serializeJSON();

            swal({
                title: 'Изпращане на поръчка',
                onOpen: () => {
                    swal.showLoading()
                }
            });

            $form.find('[type=submit]').prop('disabled', 'disabled');

            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: data,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
            }).done(function (data) {
                swal('Поръчка бе направена', 'Изчакайте изпращане на стока, номер на поръчка - ' + data.id + '!', 'success');

                $form[0].reset();
            }).fail(function (xhr) {
                if (xhr.responseJSON != undefined) {
                    swal(xhr.status.toString(), xhr.responseJSON.detail, "error");
                } else {
                    swal(xhr.status.toString(), xhr.statusText, "error");
                }
            });
        });
    });
</script>

</body>
</html>