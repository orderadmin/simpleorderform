<?php

class OrderadminAPI
{
    const TYPE_GET = 'GET';
    const TYPE_POST = 'POST';

    protected $user;
    protected $secret;
    protected $json;
    protected $result;
    protected $server = 'http://alpha.orderadmin.ru';
    protected $timeout = 15;
    protected $error;

    public function __construct($url, $user, $secret)
    {
        $this->user = 'info@reworker.bg';
        $this->secret = '123456';
    }

    public function getResult($format = true)
    {
        if ($format) {
            return json_decode($this->result, true);
        } else {
            return $this->result;
        }
    }

    public function getError()
    {
        return $this->error;
    }

    public function setRequest($array)
    {
        $this->json = json_encode($array);

        return $this;
    }

    public function setJsonRequest($json)
    {
        $this->json = $json;

        return $this;
    }

    public function getRequest()
    {
        return $this->json;
    }

    public function request($type, $url, $query = array(), $debug = false)
    {
        $ch = curl_init();

        $url = $this->server . $url;
        if (!empty($query)) {
            $url = $url . '?' . http_build_query($query);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
        ));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);


        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $this->user . ":" . $this->secret);

        if (in_array($type, array(self::TYPE_POST))) {
            $data = $this->getRequest();

            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        if ($debug) {
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        }


        $this->result = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($debug) {
            var_dump(json_decode($this->getRequest(), JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE));

            echo '<pre>';
            echo $this->result;
            print_r(curl_getinfo($ch));
            echo '</pre>';

            die();
        }


        if (in_array($httpCode, array(200, 201))) {
            return $this;
        } else {
            $result = $this->getResult();
            $description = $result['detail'] . (!empty($result['validation_messages']) ? var_export($result['validation_messages'], true) : '');

            throw new Exception($description, $result['status']);
        }
    }
}
