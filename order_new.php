<?php

namespace App;

use App\Lib\Api;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\ExceptionHandler;

chdir(dirname(__DIR__));

require_once __DIR__ . '/vendor/autoload.php';

$config = require_once('config.drone.php');

if (!file_exists($config['cacheDir'])) {
    mkdir($config['cacheDir'], 0777);
}

$api = new Api($config);

$cache = new Filesystem();
$cache->getOptions()->setTtl(3600);
$cache->getOptions()->setCacheDir($config['cacheDir']);

$plugin = new ExceptionHandler();
$plugin->getOptions()->setThrowExceptions(false);
$cache->addPlugin($plugin);

$accessToken = $cache->getItem('access_token');

if (empty($accessToken) && !empty($config['user'])
    && !empty($config['password'])
) {
    try {
        $oauth = $api->setRequest(
            [
                'client_id' => $config['client'],
                'domain' => $config['domain'],
                'grant_type' => 'password',
                'username' => $config['user'],
                'password' => $config['password'],
            ]
        )->request(Api::TYPE_POST, '/oauth', [])->getResult();

        $cache->getOptions()->setTtl($oauth['expires_in']);

        $cache->setItems($oauth);

        $accessToken = $oauth['access_token'];
    } catch (\Exception $e) {
        echo '<strong>' . $e->getMessage() . '</strong>';
    }
}

$api->setAccessToken($accessToken);

$rawData = file_get_contents("php://input");
$data = json_decode($rawData, true);

$orderPrice = 0;
foreach ($config['orderProducts'] as $orderProduct) {
    $orderPrice += $orderProduct['price'] * $orderProduct['count'];
}

$extId = $_COOKIE['oa_order_id'];
if (empty($extId)) {
    $extId = uniqid();

    setcookie('oa_order_id', $extId, time() + 60 * 5);
}

$data['shop'] = $config['shop'];
$data['extId'] = $extId;
$data['date'] = date('Y-m-d');
$data['currency'] = 4;
$data['paymentState'] = 'not_paid';
$data['orderPrice'] = $orderPrice;
$data['totalPrice'] = $orderPrice;
$data['address']['country'] = 158;
$data['recipientName'] = $_REQUEST['name'];
$data['orderProducts'] = $config['orderProducts'];
$data['profile']['extId'] = time();
$data['profile']['name'] = $_REQUEST['name'];
$data['profile']['phone'] = $_REQUEST['phone'];

try {
    $order = $api->setRequest($data)->request(
        Api::TYPE_POST, '/api/products/order', array(), false
    )->getResult();

    echo sprintf('Order number <b>%s</b>', $order['id']);
} catch (\Exception $e) {
    echo '<strong>' . $e->getMessage() . '</strong>';
}
